//
//  GenderHandler.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 22/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

struct GenderItem {
    var index: Int
    var gender: String
    var isSelected: Bool
}

class GenderHandler: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    lazy var items: [GenderItem] = {
        var items = [GenderItem]()
        var male = GenderItem(index: 0, gender: LocalizedString.MALE, isSelected: false)
        var female = GenderItem(index: 1, gender: LocalizedString.FEMALE, isSelected: false)
        items.append(male)
        items.append(female)
        return items
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GenderTableViewCell.className) as! GenderTableViewCell
        cell.genderLabel.text = item.gender
        if item.isSelected {
            cell.selectImageView.image = UIImage(named: "BlueTick")!
        } else {
            cell.selectImageView.image = nil
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        items[indexPath.row].isSelected = !items[indexPath.row].isSelected
        items
            .filter { (item) -> Bool in
                item.index != indexPath.row
            }
            .forEach { (item) in
                items[item.index].isSelected = false
            }
        tableView.reloadData()
    }
    
    func getGender() -> String? {
        return items
            .filter({ (item) -> Bool in
                return item.isSelected
            })
            .map({ (item) -> String in
                return item.gender
            })
            .first
    }
    
    func selectGender(gender: String?) {
        if var gender = gender {
            if gender.contains(where: { (char) -> Bool in
                return char == "F" || char == "f"
            }) {
                gender = LocalizedString.FEMALE
            } else {
                gender = LocalizedString.MALE
            }
            items
                .filter { (item) -> Bool in
                    return item.gender == gender
                }
                .forEach { (item) in
                    items[item.index].isSelected = true
                }
        }
    }
}

class GenderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var selectImageView: UIImageView!
    @IBOutlet weak var genderLabel: UILabel!
    
    override func awakeFromNib() {
        setRegularFont([genderLabel], 16, .black)
    }
}
