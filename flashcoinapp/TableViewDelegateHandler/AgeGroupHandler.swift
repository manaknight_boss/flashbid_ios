//
//  AgeGroupHandler.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 22/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

struct AgeGroupItem {
    var index: Int
    var range: String
    var isSelected: Bool
}

class AgeGroupHandler: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    lazy var items: [AgeGroupItem] = {
        var items = [AgeGroupItem]()
        items.append(AgeGroupItem(index: 0, range: "1 - 10", isSelected: false))
        items.append(AgeGroupItem(index: 1, range: "11 - 20", isSelected: false))
        items.append(AgeGroupItem(index: 2, range: "21 - 30", isSelected: false))
        items.append(AgeGroupItem(index: 3, range: "31 - 40", isSelected: false))
        items.append(AgeGroupItem(index: 4, range: "41 - 50", isSelected: false))
        items.append(AgeGroupItem(index: 5, range: "51+", isSelected: false))
        return items
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AgeGroupTableViewCell.className) as! AgeGroupTableViewCell
        cell.ageLabel.text = item.range
        if item.isSelected {
            cell.selectImageView.image = UIImage(named: "BlueTick")!
        } else {
            cell.selectImageView.image = nil
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        items[indexPath.row].isSelected = !items[indexPath.row].isSelected
        items
            .filter { (item) -> Bool in
                item.index != indexPath.row
            }
            .forEach { (item) in
                items[item.index].isSelected = false
            }
        tableView.reloadData()
    }
    
    func getAgeGroup() -> Int? {
        return items
            .filter({ (item) -> Bool in
                return item.isSelected
            })
            .map({ (item) -> Int in
                return item.index + 1
            })
            .first
    }
    
    func selectAgeGroup(index: Int?) {
        if let index = index, index < items.count + 1, index > 0 {
                items[index - 1].isSelected = true
        }
    }
}

class AgeGroupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var selectImageView: UIImageView!
    @IBOutlet weak var ageLabel: UILabel!
    
    override func awakeFromNib() {
        setRegularFont([ageLabel], 16, .black)
    }
}
