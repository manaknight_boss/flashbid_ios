//
//  LocalizedString.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Foundation

class LocalizedString {
    
    static let THE_PRIVACY_POLICIES_AND_TERMS_OF_SERVICES = "the_privacy_policies_and_terms_of_services".localized()
    
    static let EMAIL_VALIDATION = "email_validation".localized()
    static let PASSWORD_VALIDATION = "password_validation".localized()
    static let FIRST_NAME_VALIDATION = "first_name_validation".localized()
    static let LAST_NAME_VALIDATION = "last_name_validation".localized()
    static let PHONE_VALIDATION = "phone_validation".localized()
    
    static let ADDRESS_VALIDATION = "address_validation".localized()
    static let STATE_VALIDATION = "state_validation".localized()
    static let CITY_VALIDATION = "city_validation".localized()
    static let POSTAL_CODE_VALIDATION = "postal_code_validation".localized()
    
    static let ENTER_VALID_CODE = "enter_valid_code".localized()
    static let REGISTER_SUCCESSFUL = "register_successful".localized()
    static let SOMETHING_WENT_WRONG = "something_went_wrong".localized()
    static let MESSAGE_INTERNET_NOT_CONNECTED = "message_internet_not_connected".localized()
    static let MESSAGE_MAINTENANCE = "message_maintenance".localized()
    static let MESSAGE_PROFILE_UPDATED = "message_profile_updated".localized()
    static let MESSAGE_PASSWORD_RESET = "message_password_reset".localized()
    static let MESSAGE_NO_WINNER = "message_no_winner".localized()
    static let MESSAGE_PERFECT_SCORE = "message_perfect_score".localized()
    static let MESSAGE_CONGRATULATION_TO_WINNER = "message_congratulation_to_winner".localized()
    static let MESSAGE_INSUFFICIENT_BALANCE = "message_insufficient_balance".localized()
    
    static let PROFILE_UPDATE_DIALOG_TITLE = "profile_update_dialog_title".localized()
    static let CHOOSE_IMAGE = "choose_image".localized()
    static let CAMERA = "camera".localized()
    static let GALLERY = "gallery".localized()
    static let SCAN_CAMERA = "scan_camera".localized()
    static let SCAN_GALLERY = "scan_gallery".localized()
    static let PULL_TO_REFRESH = "pullToRefresh".localized()
    static let CANCEL = "cancel".localized()
    static let OK = "ok".localized()
    static let SCAN = "scan".localized()
    static let SUCCESS = "success".localized()
    static let ERROR = "error".localized()
    static let MALE = "male".localized()
    static let FEMALE = "female".localized()
    static let INVALID_CODE = "invalid_code".localized()

}

extension String {
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        if self.isEmpty {
            return self
        }
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
