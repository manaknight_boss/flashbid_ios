//
//  ActionTicketResponse.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 06/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct ActionTicketResponse: Decodable {
    
    var statusCode: Int
    var message: String
}
