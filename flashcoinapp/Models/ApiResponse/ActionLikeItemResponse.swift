//
//  ActionLikeItemResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 08/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct ActionLikeItemResponse: Decodable {
    
    var statusCode: Int
    var message: String
}
