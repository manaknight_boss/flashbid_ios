//
//  ActionHistoryResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//


struct ActionHistoryResponse: Decodable {
    
    var statusCode: Int
    var items: [HistoryData]
    var id: Int
    var limit: Int
}

struct HistoryData: Decodable {
    
    var id: Int
    var userId: Int
    var message: String
    var type: Int
    var createdAt: String
    var updatedAt: String
}
