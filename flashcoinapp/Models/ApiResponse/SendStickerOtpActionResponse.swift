//
//  SendStickerOtpActionResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 06/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct SendStickerOtpActionResponse: Decodable {
    
    var statusCode: Int
    var message: String
}
