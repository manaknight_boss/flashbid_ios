//
//  QRScanResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct QRScanResponse: Decodable {
    
//    if user
    var id: Int?
    var email: String?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var balance: Double?
    var profileImage: String?
    var moneyOwed: Double?
    var status: String?
    var address: String?
    var state: String?
    var country: String?
    var city: String?
    var postalCode: String?
    var type: String?
    
//    if otp or checkout
    var title: String?
    var description: String?
    var image: String?
    var amount: Double?
    var stock: Int?
    var commission: Int?
    var tax: Double?
    var discount: Double?
    var data: DataClass?
    var delivery: String?
    
//    if fotp, auction, lottery, trivia, lottery
    var secondaryImage: String?
    var initialBid: Int?
    var itemWorth: Int?
    var finalBid: Int?
    var bidAmount: Int?
    var buyAtAmount: Int?
    var ticketAmount: Int?
    var ticketTotal: Int?
    var ticketSold: Int?
    var roomId: Int?
    
//    if pay
    var updatedAt: String?
    var createdAt: String?
    var payId: Int?
    var points: Double?

//    if info
    var userId: Int?
    var name: String?
    var number: String?

//    if sticker
    var vendorId: Int?
    var allow: String?
    var used: String?
    var shopTitle: String?
    var shopCountry: String?
    var shopImage: String?
    var code: String?
    
    
    var message: String?
    var statusCode: Int?

}

struct DataClass: Decodable {
    

}





