//
//  SuccessResponse.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct SuccessResponse: Decodable {
    
    var success: Bool
}
