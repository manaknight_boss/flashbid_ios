//
//  LoyalityListResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct LoyalityListResponse: Decodable {
    
    var statusCode: Int
    var items: [LoyaltyData]
}

struct LoyaltyData: Decodable {
    
    var id: Int
    var companyName: String
    var points: Int
}
