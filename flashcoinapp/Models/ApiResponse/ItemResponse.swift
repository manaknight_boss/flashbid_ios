//
//  ItemResponse.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import Foundation

struct ItemResponse: Decodable {
    let id: Int
    let title, description, fullDescription: String?
    let image: String?
    let secondaryImage: String?
    let type: String?
    let initialBid, finalBid, bidAmount: Int?
    let marketValue: String?
    let buyAtAmount, stock, ticketAmount, ticketTotal: Int?
    let ticketSold, winnerID, roomID: Int?
    let data, status, shopTitle, shopCountry: String?
    let shopImage: String?
    let otherImages: [OtherImage]?
    var userLike: Bool?
    let amount, commission, discount, tax: Int?
}

struct OtherImage: Decodable {
    let image: String
}
