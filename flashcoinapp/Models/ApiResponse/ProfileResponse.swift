//
//  ActionProfileEditResponse.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 21/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Foundation

struct ProfileResponse: Codable {

    var profileImage: String
    var email: String
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var address: String
    var state: String
    var city: String
    var country: String
    var postalCode: String
    var qrCode: String?
    var data: String
    
    var balance: Double?
    var moneyOwed: Double?
}

struct ProfileDataResponse: Codable {
    
    var gender: String?
    var ageGroup: Int?
    
    static func decode(from dataStr: String) -> ProfileDataResponse? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            let data = dataStr.data(using: .utf8)!
            return try decoder.decode(ProfileDataResponse.self, from: data)
        } catch  {
            return nil
        }
    }
}
