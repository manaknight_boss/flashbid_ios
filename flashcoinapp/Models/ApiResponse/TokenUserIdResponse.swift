//
//  TokenResponse.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 13/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct TokenUserIdResponse: Decodable {
    
    var accessToken: String
    var userId: Int
}
