//
//  GetPayOtpDetailsResponse.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 15/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct GetPayOtpDetailsResponse: Decodable {
    
    var id: Int?
    var name: String?
    var amount: Double?
    var tax: Double?
    var discount: Double?
    var payId: Int?
    var status: String?
    var points: Double?
    var shopTitle: String?
    var shopCountry: String?
    var shopImage: String?
    var data: DataDict?
}

struct DataDict: Decodable {
    
    var threshold: Int?
    var redeem: Int?

}
