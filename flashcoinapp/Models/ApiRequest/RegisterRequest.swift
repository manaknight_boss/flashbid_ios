//
//  Register.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 13/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct RegisterRequest {
    
    var email: String
    var password: String
    var firstName: String
    var surName: String
    var phone: String
}
