//
//  ItemRequest.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct ItemRequest: Encodable {
    
    var id: Int
}
