//
//  ActionLikeItemRequest.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 08/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct ActionLikeItemRequest: Encodable {
    
    var itemId: Int
    var type: String

}
