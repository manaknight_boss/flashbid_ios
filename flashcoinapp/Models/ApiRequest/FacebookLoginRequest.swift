//
//  FacebookLoginRequest.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct FacebookLoginRequest {
    
    var email: String
    var firstName: String
    var lastName: String
    var accessToken: String
}
