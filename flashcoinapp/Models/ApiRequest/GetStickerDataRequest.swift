//
//  GetStickerDataRequest.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 07/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct GetStickerDataRequest: Encodable {
    
    var vendorId: Int
    var code: String
}
