//
//  ActionProfileImageRequest.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 21/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct ActionProfileImageRequest: Encodable {
    
    var profileImage: String
}
