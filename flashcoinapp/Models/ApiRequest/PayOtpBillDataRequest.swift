//
//  PayOtpBillDataRequest.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 08/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct PayOtpBillDataRequest {
    
    var id: Int
    var amount: Double
    var point: Double
}
