//
//  ForgotPasswordRequest.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 14/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct ForgotPasswordRequest {
    
    var email: String
}
