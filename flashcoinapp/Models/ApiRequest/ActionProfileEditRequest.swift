//
//  ActionProfileEditRequest.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 22/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct ActionProfileEditRequest: Encodable {
    
    var email: String
    var password: String?
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var address: String
    var state: String
    var city: String
    var country: String
    var postalCode: String
    var gender: String?
    var ageGroup: Int?
}
