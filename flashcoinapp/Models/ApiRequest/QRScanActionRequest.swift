//
//  QRScanActionRequest.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct ActionQRScanRequest: Encodable {
    
    var code: String
    var type: String
}
