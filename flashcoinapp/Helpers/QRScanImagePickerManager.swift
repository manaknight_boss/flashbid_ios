//
//  QRScanImagePickerManager.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Foundation
import UIKit


class QRScanImagePickerManager: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var picker = UIImagePickerController();
    var alert = UIAlertController(title: LocalizedString.CHOOSE_IMAGE, message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback : ((Int, UIImage) -> ())?;
    
    override init(){
        super.init()
        picker.navigationBar.tintColor = .c202020
    }
    
    func pickImage(_ viewController: UIViewController, _ callback: @escaping ((Int, UIImage) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;
        
        let cameraAction = UIAlertAction(title: LocalizedString.SCAN_CAMERA, style: .default){
            UIAlertAction in
            self.pickImageCallback?(1,UIImage.init())
        }
        let galleryAction = UIAlertAction(title: LocalizedString.SCAN_GALLERY, style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: LocalizedString.CANCEL, style: .destructive){
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func openGallery(){
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        self.viewController!.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        {
            pickImageCallback?(2, img)
        }
        else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            pickImageCallback?(2, img)
        }
        picker.dismiss(animated: true, completion: nil)
    }

    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
    
}
