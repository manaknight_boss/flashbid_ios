//
//  ImagePickerManager.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 31/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import LUExpandableTableView

final class ResultsExpandableTableViewSectionHeader: LUExpandableTableViewSectionHeader {
    // MARK: - Properties
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var expandCollapseButton: UIButton!
    
    
    
    override var isExpanded: Bool {
        didSet {
            // Change the image of the button when section header expand/collapse
            expandCollapseButton.setImage(isExpanded ? UIImage.init(named: "disclosure_indicator_up_results") : UIImage.init(named: "disclosure_indicator_down_results"), for: .normal)
            
        }
    }
    
    // MARK: - Base Class Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - IBActions
    
    @IBAction func expandCollapse(_ sender: UIButton) {
        // Send the message to his delegate that shold expand or collapse
        delegate?.expandableSectionHeader(self, shouldExpandOrCollapseAtSection: section)
    }
    
    // MARK: - Private Functions
    
    @objc private func didTapOnLabel(_ sender: UIGestureRecognizer) {
        // Send the message to his delegate that was selected
        delegate?.expandableSectionHeader(self, wasSelectedAtSection: section)
    }
}
