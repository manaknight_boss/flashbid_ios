//
//  ContestViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import SideMenu
import FirebaseDatabase
import RxFirebase
import RxSwift

class ContestViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let contestsHandler = ContestsHandler()
    private var refreshTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupProfile()
        setupContestsHandler()
        
        self.addTitleLogo()
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        print(Constants.ScreenSize.screenWidth)
        SideMenuManager.default.menuWidth = Constants.ScreenSize.screenWidth-80;

        NotificationCenter.default.addObserver(self, selector: #selector(sideBarItemClicked(notification:)), name: .sideBarItemClicked, object: nil)
        
        refreshTimer = Timer.scheduledTimer(withTimeInterval: 600, repeats: true, block: { (_) in
            self.collectionView.reloadData()
        })
    }
    
    deinit {
        refreshTimer.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(sideBarItemClicked(notification:)), name: .sideBarItemClicked, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .sideBarItemClicked, object: nil)
    }
    
    private func setupContestsHandler() {
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        collectionView.delegate = contestsHandler
        collectionView.dataSource = contestsHandler
        if let layout = collectionView?.collectionViewLayout as? StaggeredCollectionViewLayout {
            layout.delegate = contestsHandler
        }
        
        var stopLoading = true
        startLoading()
        contestViewModel.listenForContest()
            .do(onNext: { (contest) in
                print("\nDebugging Contest FC ID: \(contest.id) and FK: \(contest.firebaseKey)")
            })
            .do(onNext: { (contest) in
                let show = contest.status != .COMPLETE && contest.status != .LOST && contest.status != .CLAIMED
                if let oldContestIndex = self.contestsHandler.contests.firstIndex(where: { $0.id == contest.id }) {
                    print("\nDebugging Contest Updating contest with id: \(contest.id) and firebaseKey: \(contest.firebaseKey)")
                    if show {
                        Contest.copy(from: contest, to: &self.contestsHandler.contests[oldContestIndex])
                    } else {
                        self.contestsHandler.contests.remove(at: oldContestIndex)
                    }
                } else if show {
                    print("\nDebugging Contest Adding contest with id: \(contest.id) and firebaseKey: \(contest.firebaseKey)")
                    if contest.size.equalIgnoreCase("small") {
                        self.contestsHandler.contests.append(contest)
                    } else {
                        self.contestsHandler.contests.insert(contest, at: 0)
                    }
                }
            })
            .debounce(1, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { contest in
                if stopLoading {
                    self.stopLoading()
                    stopLoading = false
                }
                print("\nDebugging Contest Reloading contests..")
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
        
        contestsHandler.itemClickHandler = { contest in
            let vc = ContestDetailViewController.instantiate(storyboardName: "Contest")
            vc.contest = contest
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func sideBarItemClicked(notification: Notification) {
        self.sideBarItemClicked1(notification: notification)
    }
    
    private func setupProfile() {
        startLoading()
        profileViewModel.getProfile(accessToken: accessToken, { () in
            self.stopLoading()
            self.listenProfileChanges()
        }, errorHandler)
    }
    
    private func listenProfileChanges() {
        profileViewModel.listenProfileChanges()
            .subscribe(onNext: { snapshot in
                self.profileViewModel.parseFirebaseUser(snapshot: snapshot)
            })
            .disposed(by: disposeBag)
    }
}
