//
//  QRCodeViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 20/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {

    var screenTitle = ""

    @IBOutlet weak var qrCodeImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.screenTitle

        if let qrCode = profileViewModel.qrCode {
            let image = generateQRCode(from: qrCode)
            self.qrCodeImageView.image = image
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
