//
//  ServicesViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appTableView: UITableView!
    // array of dicts
    let titlesArray = ["Recargas","Encuestas","Fidelidad"]
    let detailsArray = ["Recarga tus minutos", "Gana Puntos por tu opinión", "Consulta tus membresías de fidelidad"]
    let imagesArray = ["phoneshare-app", "poll-app", "fidelidad-app"]

    let cellReuseIdentifier = "appsCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addTitleLogo()

        self.appTableView.tableFooterView = UIView(frame: .zero)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(sideBarItemClicked(notification:)), name: .sideBarItemClicked, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .sideBarItemClicked, object: nil)
    }
    
    @objc func sideBarItemClicked(notification: Notification) {
        self.sideBarItemClicked1(notification: notification)
    }
    
    // MARK: - TableView Methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titlesArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
            as! AppsTableViewCell

        cell.rowTitle.text = self.titlesArray[indexPath.row]
        cell.rowDetails.text = self.detailsArray[indexPath.row]
        cell.rowImage?.image = UIImage.init(named: self.imagesArray[indexPath.row])
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 2 {
            self.pushToLoyaltyViewController(index: indexPath.row)
        }
    }
    
    func pushToLoyaltyViewController(index:Int){
        
        let itemName = self.titlesArray[index]
        let vcName = UIStoryboard(name: Constants.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.LoyaltyListInServicesViewController) as! LoyaltyListInServicesViewController
        vcName.screenTitle = itemName
        vcName.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vcName, animated: true)
    }

}
