//
//  PayOtpViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit

class PayOtpViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var cashRegisterImgView: UIImageView!
    @IBOutlet weak var payBillView: UIView!
    @IBOutlet weak var pricingTableView: UITableView!
    @IBOutlet var itemHeader: UIView!
    
    @IBOutlet var totalFooter: UIView!
    @IBOutlet weak var tableTitleView: UIView!
    
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var shopCountryLbl: UILabel!
    
    @IBOutlet weak var itemTotalPriceLbl: UILabel!
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    
    var id = 0
    var isLoyaltyRedeemed = false
    var item = GetPayOtpDetailsResponse()
    let rowTitles = ["Sub total", "Descuento", "Impuestos"]
    var rowPrices = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        cashRegisterImgView.layer.borderWidth = 0.5
        cashRegisterImgView.layer.borderColor = UIColor.black.cgColor
        
        tableTitleView.layer.borderWidth = 0.5
        tableTitleView.layer.borderColor = UIColor.black.cgColor
        
        payBillView.layer.shadowColor = UIColor.black.cgColor
        payBillView.layer.shadowOpacity = 0.7
        payBillView.layer.shadowOffset = CGSize.zero
        
        self.itemHeader.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.screenWidth, height: self.itemHeader.bounds.height)
        self.pricingTableView.tableHeaderView = self.itemHeader
        
        self.totalFooter.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.screenWidth, height: self.totalFooter.bounds.height)
        self.pricingTableView.tableFooterView = self.totalFooter
        
        self.view.subviews.forEach { $0.isHidden = true }
        self.getPayOtpDetails()

        // Do any additional setup after loading the view.
    }
    
    func fillUIDetails(){
        DispatchQueue.main.async {
            self.shopImageView.kf.setImage(with: URL.init(string: self.item.shopImage ?? ""))
            self.shopNameLbl.text = self.item.shopTitle
            self.shopCountryLbl.text = self.item.shopCountry
            self.itemNameLbl.text = self.item.name
            
            // check threshold should be greater than ZERO (to avail redeem or points)
            if self.item.data?.threshold ?? 0 > 0{
                if self.item.data?.redeem ?? 0 > 0{
                    self.item.points = Double(self.item.data?.redeem ?? 0)
                    self.showLoyaltyRedeemAlert(self.item.points ?? 0.0)
                }
                
                else if self.item.points != 0.0{
                    // if threshold is LESS THAN points, assign threshold value as Points, because max points user can redeem should be equal to or less than threshold
                    if self.item.data?.threshold ?? 0 < Int(self.item.points ?? 0.0){
                        self.item.points = Double(self.item.data?.threshold ?? 0)
                    }

                    self.showLoyaltyRedeemAlert(self.item.points ?? 0.0)
                }

            }
            
            self.updatePricingLabels()
            self.view.subviews.forEach { $0.isHidden = false }

        }
    }
    
    func updatePricingLabels(){
        let itemAmount = self.item.amount ?? 0.0
        let itemDiscount = self.item.discount ?? 0.0
        let itemTax = self.item.tax ?? 0.0
        var itemLoyaltyPoints = 0.0
        
        self.rowPrices.removeAll()

        self.rowPrices.append(String(format: "%.2f", itemAmount))
        self.rowPrices.append(String(format: "%.2f", itemDiscount))

        // if redeeming loyalty points, add them to discount row
        if isLoyaltyRedeemed {
            itemLoyaltyPoints = self.item.points ?? 0.0
            self.rowPrices.removeLast()
            self.rowPrices.append(String(format: "%.2f", itemDiscount + itemLoyaltyPoints))
        }
        
        self.rowPrices.append(String(format: "%.2f", itemTax))
        
        self.itemTotalPriceLbl.text = String(format: "%.2f", itemAmount + itemTax - itemDiscount - itemLoyaltyPoints)
        
        self.itemPriceLbl.text = self.itemTotalPriceLbl.text
        
        self.pricingTableView.reloadData()

    }
    
    func getPayOtpDetails(){
        
        let request = GetPayOtpDataRequest.init(id: self.id)
        
        let respParam = ["id", "name", "amount", "tax", "discount", "pay_id", "status", "points", "shop_title", "shop_country", "shop_image","data"]
        
        startLoading()
        
        qrScanViewModel.callToGetPayOtpDataDetails(accessToken: accessToken , responseParam: respParam, request: request, { (response) in
            self.stopLoading()
            self.item = response
            self.fillUIDetails()
            
        }) { (ErrorResponse) in
            self.errorHandler(error: ErrorResponse)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.navigationController?.popToRootViewController(animated: false)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    @IBAction func payBillBtnPressed(_ sender: Any) {
        
        let amountToBePaid = Double(self.itemTotalPriceLbl.text ?? "0.0")
        
        // if user balance is less than amount to be paid, show error msg and return
        if Int(profileViewModel.profileData.value?.balance ?? 0.0) < Int(amountToBePaid ?? 0.0)  {
            self.showErrorDialog(message: LocalizedString.MESSAGE_INSUFFICIENT_BALANCE)
            return
        }
        
        startLoading()
        let request = PayOtpBillDataRequest.init(id: self.item.id ?? 0, amount: amountToBePaid ?? 0.0, point: isLoyaltyRedeemed ? self.item.points ?? 0.0 : 0.00)
        
        qrScanViewModel.callToPayOtpBillDataDetails(accessToken: accessToken, request: request, { (response) in
            self.stopLoading()
            if response.statusCode == 200{
                self.showAlertMsgAndPopToRoot(response.message)
            }
            
        }) { (ErrorResponse) in
            self.errorHandler(error: ErrorResponse)
        }

    }
    
    // MARK: - TableView Methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowPrices.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "payCell", for: indexPath)
            as! PayOtpTableViewCell
        
        cell.itemLbl.text = self.rowTitles[indexPath.row]
        
        // show "-" sign with discount
        if indexPath.row == 1 {
            cell.priceLbl.text = "-\(self.rowPrices[indexPath.row])"
        }
        else{
            cell.priceLbl.text = self.rowPrices[indexPath.row]
        }

        return cell
    }
    
    func showLoyaltyRedeemAlert(_ points:Double){
        let alert = UIAlertController.init(title: "Puntos de Fidelidad", message: "Tienes \(points) puntos de lealtad con nosotros ¿deseas canjearlos?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (UIAlertAction) in
            print("yes")
            self.isLoyaltyRedeemed = true
            self.updatePricingLabels()

        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}
