//
//  AboutViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 20/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController,UIWebViewDelegate {

    let loading = Loading()
    var screenTitle = ""

    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var myWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.screenTitle
        activitySpinner.startAnimating()

        let url = URL (string: "https://www.flashbidrd.com/")
        let requestObj = URLRequest(url: url!)
        self.myWebView.loadRequest(requestObj)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - UIWebView Methods
    func webViewDidStartLoad(_ webView: UIWebView) {
        activitySpinner.isHidden = false
        activitySpinner.startAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        print(error.localizedDescription)
        activitySpinner.isHidden = true
        activitySpinner.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activitySpinner.isHidden = true
        activitySpinner.stopAnimating()
    }

}
