//
//  StickerOtpViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit

class StickerOtpViewController: BaseViewController {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var shopCountry: UILabel!
    @IBOutlet weak var shopTitle: UILabel!
    @IBOutlet weak var shopImageView: UIImageView!
    
    @IBOutlet weak var stickerStaticImage: UIImageView!
    var stickerVendorId = 0
    var stickerCode = ""
    var sticker = QRScanResponse()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stickerStaticImage.layer.borderWidth = 0.5
        self.stickerStaticImage.layer.borderColor = UIColor.black.cgColor
        self.view.subviews.forEach { $0.isHidden = true }
        self.getStickerOtpDetails()
    }
    
    func fillUIDetails(){
        DispatchQueue.main.async {
            self.priceLbl.text = "($\(self.sticker.amount ?? 0))"
            self.amountLbl.text = "Canjear \(self.sticker.amount ?? 0) FP"
            self.shopTitle.text = self.sticker.shopTitle
            self.shopCountry.text = self.sticker.shopCountry
            self.shopImageView.kf.setImage(with: URL.init(string: self.sticker.shopImage ?? ""))
            self.view.subviews.forEach { $0.isHidden = false }
        }
    }
    
    func getStickerOtpDetails(){
        
        let request = GetStickerDataRequest.init(vendorId: self.stickerVendorId, code: self.stickerCode)

        let respParam = ["id", "vendor_id", "amount", "shop_title", "shop_country", "shop_image"]
        startLoading()
        
        qrScanViewModel.callToGetStickerDataDetails(accessToken: accessToken , responseParam: respParam, request: request, { (response) in
            self.stopLoading()

            if response.statusCode != 404{
                self.sticker = response
                self.fillUIDetails()
            }
            else{
                self.showAlertMsgAndPopToRoot(response.message ?? LocalizedString.SOMETHING_WENT_WRONG)
            }
            
        }) { (ErrorResponse) in
            self.errorHandler(error: ErrorResponse)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.navigationController?.popToRootViewController(animated: false)
            }
        }  
    }
    

    @IBAction func backBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)

    }
    @IBAction func buyBtnPressed(_ sender: Any) {
        startLoading()
        
        stickerOtpActionViewModel.sendStickerOtpData(accessToken: accessToken, id: sticker.id ?? 0, codeType: Constants.QRType.sticker, code: self.stickerCode, amount: sticker.amount ?? 0, vendorId: sticker.vendorId ?? 0, { (response) in
            self.stopLoading()
            if response.statusCode == 200{
                self.showAlertMsgAndPopToRoot(response.message)
            }
            
        }) { (ErrorResponse) in
            self.errorHandler(error: ErrorResponse)
        }
        
    }
    

    
}
