
//
//  UserOtpViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit

class UserOtpViewController: UIViewController {
    
    var user = QRScanResponse()

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fillUIDetails()
        // Do any additional setup after loading the view.
    }
    
    func fillUIDetails(){
        self.nameLbl.text = user.firstName! + " " + user.lastName!
        self.emailLbl.text = user.email
        self.userImageView.kf.setImage(with: URL.init(string: user.profileImage ?? ""))
        self.userImageView.kf.indicatorType = .activity
    }
    
    
    // MARK:- Button Actions
    @IBAction func backBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)
    }

}
