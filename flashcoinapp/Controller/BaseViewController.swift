//
//  BaseViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 21/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController, Storyboarded {
    
    private lazy var loader: Loading = {
        let loading = Loading()
        loading.create(into: self.view)
        return loading
    }()
    private let loaderDispatchQueue = DispatchQueue(label: "loding")
    private var loaderCount = 0
    
    let disposeBag = DisposeBag()
    
    func startLoading() {
        loaderDispatchQueue.sync {
            loader.start()
            loaderCount += 1
        }
    }
    
    func stopLoading() {
        loaderDispatchQueue.sync {
            if loaderCount > 0 {
                loaderCount -= 1
                if loaderCount == 0 {
                    loader.stop()
                }
            }
        }
    }
    
    func errorHandler(error: ErrorResponse) {
        loader.stop()
        showErrorDialog(message: error.message ?? LocalizedString.SOMETHING_WENT_WRONG)
    }
    
    func connected() -> Bool {
        let connected = Connection.isConnectedToNetwork()
        if !connected {
            showErrorDialog(message: LocalizedString.MESSAGE_INTERNET_NOT_CONNECTED)
        }
        return connected
    }
    
    func addTitleLogo(){
        let logoImage:UIImage = UIImage.init(named: "FlashbidToolbarTitle")!
        self.navigationItem.titleView = UIImageView(image: logoImage)
    }
    
    @IBAction func scanIconTapped(_ sender: Any) {
        self.pushToQRScanningViewController()
    }
    
    func pushToQRScanningViewController(){
        QRScanImagePickerManager().pickImage(self) { (value, image) in
            
            let vcName = UIStoryboard(name: Constants.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.QRScannerViewController) as! QRScannerViewController
            vcName.selectedScanOption = value
            vcName.selectedImage = image
            vcName.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vcName, animated: true)
        }
    }
    
    func showAlertMsgAndPopToRoot(_ msg: String){
        let alert = UIAlertController.init(title: "", message: msg, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction.init(title: LocalizedString.OK, style: .default, handler: { (UIAlertAction) in
            self.navigationController?.popToRootViewController(animated: false)
        }))
    }
    
    func sideBarItemClicked1(notification: Notification) {
        if let item = notification.userInfo?[Constants.String.item] as? String {
            var itemName = ""
            if item == Constants.Controller.ProfileViewController {
                itemName = notification.userInfo?[Constants.String.itemName] as? String ?? ""
                let vcName = UIStoryboard(name: Constants.Storyboard.Profile, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.ProfileViewController) as! ProfileViewController
                vcName.screenTitle = itemName
                vcName.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            else if item == Constants.Controller.HistoryViewController {
                itemName = notification.userInfo?[Constants.String.itemName] as? String ?? ""
                let vcName = UIStoryboard(name: Constants.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.HistoryViewController) as! HistoryViewController
                vcName.screenTitle = itemName
                vcName.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            else if item == Constants.Controller.AboutViewController {
                itemName = notification.userInfo?[Constants.String.itemName] as? String ?? ""
                let vcName = UIStoryboard(name: Constants.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.AboutViewController) as! AboutViewController
                vcName.screenTitle = itemName
                vcName.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            else if item == Constants.Controller.QRCodeViewController {
                itemName = notification.userInfo?[Constants.String.itemName] as? String ?? ""
                let vcName = UIStoryboard(name: Constants.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.QRCodeViewController) as! QRCodeViewController
                vcName.screenTitle = itemName
                vcName.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            else if item == Constants.Controller.QRScannerViewController {
                self.pushToQRScanningViewController()
            }
            else if item == Constants.Controller.LogOut {
                startLoading()
                AuthRepository.shared.actionLogOut(accessToken: accessToken, { (response) in
                    self.stopLoading()
                    Login.saveLoginInformation(accessToken: nil, userId: 0)
                    self.startCoordinator.logout()
                }, errorHandler)
            }
        }
    }
}
