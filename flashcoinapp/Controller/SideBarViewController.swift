//
//  SideBarViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 18/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import Firebase
import RxFirebase

class SideBarViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var sideBarTableView: UITableView!
    let sideBarItemsArray: [String] = ["Editar Perfil", "Mi Billetera", LocalizedString.SCAN, "Historial", "Mi Código", "Nosotros", "Cerrar"]
    let sideBarItemsImagesArray: [String] = ["edit_icon", "flashpoint_icon_white", "qr_icon_grey", "history_icon_grey", "qr_icon_grey", "info_icon_grey", "exit_icon_grey"]

    let cellReuseIdentifier = "sideBarCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        listenForProfileChanges()
    }
    
    // MARK: - TableView Methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sideBarItemsArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)

        // set the text and image from the data model
        cell.textLabel?.text = self.sideBarItemsArray[indexPath.row]
        cell.textLabel?.font = UIFont.init(name: "Exo2-Regular", size: 16.0)
        cell.imageView?.image = UIImage.init(named: self.sideBarItemsImagesArray[indexPath.row])
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        dismiss(animated: true, completion: nil)
        
        self.sideMenuRowTapped(rowIndex: indexPath.row)

    }
    
    func sideMenuRowTapped(rowIndex:Int){
        if rowIndex == 0 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.ProfileViewController, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        }
        else if rowIndex == 2 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.QRScannerViewController, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        }
        else if rowIndex == 3 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.HistoryViewController, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        }
        else if rowIndex == 4 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.QRCodeViewController, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        }
        else if rowIndex == 5 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.AboutViewController, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        } else if rowIndex == 6 {
            let itemDict:[String: String] = [Constants.String.item: Constants.Controller.LogOut, Constants.String.itemName: self.sideBarItemsArray[rowIndex]]
            NotificationCenter.default.post(name: .sideBarItemClicked, object: nil, userInfo: itemDict)
        }
    }
}

// MARK: Profile Handling
extension SideBarViewController {
    
    func listenForProfileChanges() {
        
        profileViewModel.image
            .asObservable()
            .subscribe(onNext: { image in
                self.profileImageView.image = image
            })
            .disposed(by: disposeBag)
        
        profileViewModel.profileData
            .asObservable()
            .subscribe(onNext: { profileData in
                if let data = profileData {
                    self.balanceLabel.text = String(format: "%.2f", data.balance ?? 0)
                    self.nameLabel.text = data.firstName + " " + data.lastName
                }
            })
            .disposed(by: disposeBag)
    }
}
