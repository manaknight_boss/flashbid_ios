//
//  LandingTabBarViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 17/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class LandingTabBarViewController: UITabBarController, Storyboarded {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

}

extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 60 // adjust your size here
        return sizeThatFits
    }
}
