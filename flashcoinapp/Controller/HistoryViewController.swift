//
//  HistoryViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var noHistoryLabel: UILabel!
    @IBOutlet weak var historyTableView: UITableView!
    let cellReuseIdentifier = "historyCell"
    var screenTitle = ""
    let loading = Loading()
    var refreshControl: UIRefreshControl!
    var listId = 0
    var historyData = [HistoryData]()
    var isLoadingMoreItems = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.historyTableView.tableFooterView = UIView(frame: .zero)
        self.navigationItem.title = self.screenTitle
        
        loading.create(into: view)
        loading.start()
        
        callHistoryService()
        
        // add pull to refresh in tableview
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: LocalizedString.PULL_TO_REFRESH)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.historyTableView.addSubview(refreshControl)
    }
    
    func callHistoryService() {
        isLoadingMoreItems = true

        historyViewModel.getHistory(accessToken: accessToken, id: self.listId, { (history) in
            self.loading.stop()
            self.refreshControl.endRefreshing()
            // empty array if list id = 0
            if self.listId == 0 {
                self.historyData.removeAll()
            }
            self.listId = history.id
            

            self.historyData += history.items
            
            if self.historyData.count >  0{
                self.historyTableView.reloadData()
                self.noHistoryLabel.isHidden = true
            }
            else{
                if self.listId == 0{
                    self.noHistoryLabel.isHidden = false
                }
                else{
                    self.noHistoryLabel.isHidden = true
                }
            }
            self.isLoadingMoreItems = false

        }, { (error) in
            self.loading.stop()
            self.refreshControl.endRefreshing()
            self.showErrorDialog(message: error.message ?? LocalizedString.SOMETHING_WENT_WRONG)
            self.isLoadingMoreItems = false
        }
        )
    }
    
    @objc func refresh(_ sender: Any) {
        listId = 0
        self.callHistoryService()
    }
    
    // MARK: - TableView Methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyData.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            // create a new cell if needed or reuse an old one
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
                as! HistoryTableViewCell
            
            let historyItem = self.historyData[indexPath.row]
            
            // to avoid crash, if array index out of bounds for historyTypes, range 1-9
            if historyItem.type > 0 && historyItem.type <= 9 {
                cell.historyTitle.text = Constants.historyTypes[historyItem.type-1]
            }
            
            cell.historyInfo.text = historyItem.message
            cell.historyImageView.image = setImageInCell(type: historyItem.type)
        
        if indexPath.row == self.historyData.count-1 && isLoadingMoreItems == false {
            self.loadMoreItems()
        }
            return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func setImageInCell(type:Int) -> UIImage{
        
        var imageName = ""
        switch type {
        case 1:
            imageName = "subastaFlash"
        case 2:
            imageName = "sorteo"
        case 3:
            imageName = "fP"
        case 4:
            imageName = "info"
        case 5:
            imageName = "tienda"
        case 7:
            imageName = "subasta"
        case 8:
            imageName = "trivialidades"
        case 9:
            imageName = "fidelidad"
        default:
            imageName = "fidelidad"
        }
        
        return UIImage.init(named: imageName)!
    }
    
    func loadMoreItems(){
        if self.historyData.count % 20 != 0 {
            return
        }
        self.callHistoryService()
    }
}
