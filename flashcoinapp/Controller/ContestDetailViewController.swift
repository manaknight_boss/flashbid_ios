//
//  ContestDetailViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit
import RxSwift

class ContestDetailViewController: BaseViewController {
    
    public var contest: Contest!
    
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var shopTitleLabel: UILabel!
    @IBOutlet weak var shopCountryLabel: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var imagePageController: UIPageControl!
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titlePriceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet weak var ticketMessageLabel: UILabel!
    @IBOutlet weak var fullDescriptionTitleLabel: UILabel!
    @IBOutlet weak var fullDescriptionLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ticketAmountIcon: UIImageView!
    @IBOutlet weak var ticketAmountLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var progressBar: ContestProgressBar!
    @IBOutlet weak var buyButton: UIButton!
    // Status queue
    @IBOutlet weak var queueMessageLabel: UILabel!
    // Status flash
    @IBOutlet weak var flashButton: UIButton!
    // FOTP
    @IBOutlet weak var buyAtAmoutIcon: UIImageView!
    @IBOutlet weak var buyAtAmoutLabel: UILabel!
    @IBOutlet weak var buyAtButton: UIButton!
    
    private let bottomViewHeight: CGFloat = 65
    private var contestDetailImageHandler = ContestDetailImageHandler()
    
    private var showProgressView = false {
        didSet {
            ticketAmountIcon.isHidden = !showProgressView
            ticketAmountLabel.isHidden = !showProgressView
            percentageLabel.isHidden = !showProgressView
            progressBar.isHidden = !showProgressView
            buyButton.isHidden = showProgressView ? progressPercentage == 100 : true
           
        }
    }
    private var showQueueView = false {
        didSet {
            queueMessageLabel.isHidden = !showQueueView
        }
    }
    private var showFlashView = false {
        didSet {
            flashButton.isHidden = !showFlashView
        }
    }
    private var showOtpView = false {
        didSet {
            buyAtAmoutIcon.isHidden = !showOtpView
            buyAtAmoutLabel.isHidden = !showOtpView
            buyAtButton.isHidden = !showOtpView
        }
    }
    private func showBottomViews(
        progressView: Bool = false,
        queueView: Bool = false,
        flashView: Bool = false,
        otpView: Bool = false
        ) {
        
        showProgressView = progressView
        showQueueView = queueView
        showFlashView = flashView
        showOtpView = otpView
    }
    
    private var ticketTotal = 0
    private var isCompleteClaimedOrLost: Bool {
        get {
            return status.equalIgnoreCase([ItemStatus.CLAIMED, ItemStatus.COMPLETE, ItemStatus.LOST])
        }
    }
    private var isInQueue: Bool {
        get {
            return status.equalIgnoreCase(ItemStatus.QUEUE)
        }
    }
    private var isInFlash: Bool {
        get {
            return status.equalIgnoreCase(ItemStatus.FLASH)
        }
    }
    private var progressPercentage: Float = 0 {
        didSet {
            percentageLabel.text = "\(Int(progressPercentage))% hacia el sorteo"
            progressBar.progress = max(progressPercentage, 10)
            if progressPercentage == 100 {
                buyButton.isHidden = true
            }
        }
    }
    private var ticketSold = 0 {
        didSet {
            if !isCompleteClaimedOrLost && contest.type != .OTP && contest.type != .FOTP {
                let progressPercentage = divide(ticketSold, by: ticketTotal) * 100
                self.progressPercentage = progressPercentage
            }
        }
    }
    
    private var status = ItemStatus.ACTIVE {
        didSet {
            if let type = contest.type {
                switch type {
                    
                case .LOTTERY:
                    title = "Sorteo"
                    progressBar.bgColor = .cdf4f41
                    typeImageView.setImage(#imageLiteral(resourceName: "sorteo_item"))
                    if isCompleteClaimedOrLost {
                        progressPercentage = 100
                    }
                    showBottomViews(progressView: true)
                case .SPECIAL_AUCTION:
                    title = "Subasta Flash"
                    progressBar.bgColor = .c313fa7
                    typeImageView.setImage(#imageLiteral(resourceName: "spl_auction_item"))
                    if isCompleteClaimedOrLost {
                        progressPercentage = 100
                    }
                    if isInFlash {
                        showBottomViews(flashView: true)
                    }
                    else if isInQueue {
                        showBottomViews(queueView: true)
                    }
                    else {
                        showBottomViews(progressView: true)
                    }
                    
                case .FOTP, .OTP:
                    title = "Tienda"
                    typeImageView.setImage(#imageLiteral(resourceName: "otp"))
                    showBottomViews(otpView: true)
                    
                case .AUCTION:
                    print("AUCTION")
                    
                case .TRIVIALITIES:
                    print("TRIVIALITIES")
                }
            }
        }
    }
    
    private var itemResponse: ItemResponse! {
        didSet {
            
            bottomViewHeightConstraint.constant = bottomViewHeight
            
            // Setting shop UI
            shopImageView.setImage(itemResponse.shopImage)
            shopTitleLabel.text = itemResponse.shopTitle
            shopCountryLabel.text = itemResponse.shopCountry
            
            // Setting images
            var images = [String]()
            if let image = itemResponse.type == ItemType.OTP
                ? itemResponse.image
                : itemResponse.secondaryImage {
                
                images.append(image)
            }
            if let otherImages = itemResponse.otherImages {
                otherImages.forEach { (image) in
                    images.append(image.image)
                }
            }
            contestDetailImageHandler.images = images
            
            // Setting images bottom description
            titleLabel.text = itemResponse.title
            titlePriceLabel.text = "($\(itemResponse.type == ItemType.OTP ? String(calculateAmountForOtp(with: itemResponse)) : itemResponse.marketValue ?? ""))"
            heartImageView.image = itemResponse.userLike! ? #imageLiteral(resourceName: "black_heart") : #imageLiteral(resourceName: "heart")
            shareImageView.image = #imageLiteral(resourceName: "share")
            descriptionLabel.text = itemResponse.description
            
            // Setting grey area
            switch itemResponse.type {
                
            case ItemType.LOTTERY:
                
                let ticketMessageBoldPart = "\(itemResponse.ticketAmount ?? 0) FP."
                let ticketMessage = "Compra una boleta y estarás participando en el Sorteo por solo \(ticketMessageBoldPart) No olvides confirmar tu participación en tu historial, Buena Suerte!"
                
                let attributedText = NSMutableAttributedString(string: ticketMessage)
                attributedText.addAttributes(FontHelper.regular(fontSize: 14).textAttributes(textColor: .black), range: ticketMessage.range(of: ticketMessage)!.nsRange)
                attributedText.addAttributes(FontHelper.bold(fontSize: 14).textAttributes(textColor: .black), range: ticketMessage.range(of: ticketMessageBoldPart)!.nsRange)
                ticketMessageLabel.attributedText = attributedText
                
            case ItemType.SPECIAL_AUCTION:
                
                ticketMessageLabel.text = "Compra una boleta y reserva un asiento para la Subasta Flash, el postor mas alto al final de 60 Segundos se lleva el producto!"
                
            case ItemType.FOTP, ItemType.OTP:
                
                ticketMessageLabel.text = (itemResponse.stock ?? 0) > 0 ? "Inventario: Disonible" : "Inventario Agotado"
                
            default:
                print("Invalid item type got in ContestDetailViewController")
                
            }
            fullDescriptionTitleLabel.text = "Información detallada:"
            fullDescriptionLabel.text = itemResponse.fullDescription
            
            // Setting progress bar container views
            ticketAmountLabel.text = "\(itemResponse.ticketAmount ?? 0)"
            ticketTotal = itemResponse.ticketTotal ?? 0
            
            // Setting FOTP
            buyAtAmoutLabel.text = "\(itemResponse.type == ItemType.OTP ? (calculateAmountForOtp(with: itemResponse)) : itemResponse.buyAtAmount ?? 0)"
            
            status = contest.status?.rawValue ?? ItemStatus.ACTIVE
            print("\nDebugging Contest Listening data for contest with id: \(contest.id) and firebaseKey: \(contest.firebaseKey)")
            listenFirebaseWith(firebaseKey: contest.firebaseKey, id: contest.id)
        }
    }
    
    func calculateAmountForOtp(with itemResponse: ItemResponse) -> Int {
        let amount = itemResponse.amount ?? 0
        let commission = itemResponse.commission ?? 0
        let discount = itemResponse.discount ?? 0
        let tax = itemResponse.tax ?? 0
        return amount + commission - discount + tax
    }
    
    func listenFirebaseWith(firebaseKey: String?, id: Int?) {
        if let firebaseKey = firebaseKey {
            contestViewModel.listenForContest(firebaseKey: firebaseKey)
                .asObservable()
                .subscribe(onNext: { contest in
                    self.listenFirebaseContestChanges(contest: contest)
                })
                .disposed(by: disposeBag)
        }
        else if let id = id {
            contestViewModel.listenForContest(id: id)
                .asObservable()
                .subscribe(onNext: { contest in
                    self.listenFirebaseContestChanges(contest: contest)
                })
                .disposed(by: disposeBag)
        }
    }
    
    func listenFirebaseContestChanges(contest: Contest) {
        self.contest = contest
        status = contest.status?.rawValue ?? ItemStatus.ACTIVE
        ticketTotal = contest.ticketTotal
        ticketSold = contest.ticketSold
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideBottomView()
        setupFonts()
        setupTitleAndRightButton()
        setupContestDetailImageHandler()
        getContestDetail()
    }
    
    func hideBottomView() {
        bottomView.subviews
            .forEach({ $0.isHidden = true })
        bottomViewHeightConstraint.constant = 0
    }
    
    func setupFonts() {
        setRegularFont([navigationController?.navigationBar as Any], 16, .c202020)
        setMediumFont([shopTitleLabel], 16, .c202020)
        setLightFont([shopCountryLabel, descriptionLabel], 16, .c202020)
        setBoldFont([titleLabel], 16, .c202020)
        setRegularFont([titlePriceLabel, ticketMessageLabel], 14, .black)
        setBoldFont([fullDescriptionTitleLabel], 14, .black)
        setRegularFont([fullDescriptionLabel], 15, .black)
        setRegularFont([buyButton, flashButton, buyAtButton], 15, .white)
        setRegularFont([ticketAmountLabel, queueMessageLabel, buyAtAmoutLabel], 16, .black)
        setRegularFont([percentageLabel], 10, .c202020)
    }
    
    func setupTitleAndRightButton() {
        if let type = contest.type {
            switch type {
                
            case .LOTTERY:
                title = "Sorteo"
                
            case .SPECIAL_AUCTION:
                title = "Subasta Flash"
                
            case .FOTP, .OTP:
                title = "Tienda"
                
            case .AUCTION:
                title = "AUCTION"
                
            case .TRIVIALITIES:
                title = "TRIVIALITIES"
            }
        }
        let walletButton = UIBarButtonItem(title: "Billetera", style: .plain, target: self, action: #selector(onBilletera))
        setRegularFont([walletButton], 16, .black)
        navigationItem.setRightBarButton(walletButton, animated: true)
    }
    
    @objc func onBilletera() {
        print("onBilletera")
    }
    
    func setupContestDetailImageHandler() {
        imageCollectionView.delegate = contestDetailImageHandler
        imageCollectionView.dataSource = contestDetailImageHandler
        imageCollectionView.isPagingEnabled = true
        contestDetailImageHandler.imagePageController = imagePageController
        contestDetailImageHandler.notifier = {
            self.imageCollectionView.layer.borderWidth = 0.5
            self.imageCollectionView.layer.borderColor = UIColor.black.cgColor
            self.imageCollectionView.reloadData()
        }
    }
    
    func getContestDetail() {
        startLoading()
        contestViewModel.getContestDetail(accessToken: accessToken, request: ItemRequest(id: contest.id), isOtp: contest.type == .OTP, { (response) in
            self.stopLoading()
            self.itemResponse = response
        }, errorHandler)
    }
    
    @IBAction func buy(_ sender: Any) {
        
        print("\nDebugging Contest Bying ticket for contest with id: \(contest.id) and firebaseKey: \(contest.firebaseKey)")
        
        guard let type = contest.type else { return }
        
        switch type {
            
        case .LOTTERY, .SPECIAL_AUCTION:
            
            if (contest.ticketAmount) > Int(profileViewModel.profileData.value?.balance ?? 0) {
                showErrorDialog(message: LocalizedString.MESSAGE_INSUFFICIENT_BALANCE)
            }
            
            startLoading()
            contestViewModel.buyTicket(accessToken: accessToken, request: ActionTicketRequest(id: contest.id), { (response) in
                self.stopLoading()
                self.showSuccessDialog(message: response.message) { _ in
                    self.navigationController?.popViewController(animated: true)
                }
            }, errorHandler)
            
        case .FOTP, .OTP:
            
            if (contest.type == .OTP ? (calculateAmountForOtp(with: itemResponse)) : contest.buyAtAmount) > Int(profileViewModel.profileData.value?.balance ?? 0) {
                showErrorDialog(message: LocalizedString.MESSAGE_INSUFFICIENT_BALANCE)
            }
            
            startLoading()
            contestViewModel.buyOtp(accessToken: accessToken, id: contest.id, type: contest.type?.rawValue ?? "", { (response) in
                self.stopLoading()
                self.showSuccessDialog(message: response.message) { _ in
                    self.navigationController?.popViewController(animated: true)
                }
            }, errorHandler)
            
        default:
            print("Invalid item type got in ContestDetailViewController")
            
        }
    }
    
    @IBAction func heartIconTapped(_ sender: UITapGestureRecognizer) {
        self.itemResponse.userLike = !self.itemResponse.userLike!

        contestViewModel.likeItem(accessToken: accessToken, request: ActionLikeItemRequest(itemId: contest.id, type: contest.type?.rawValue ?? ""), { (response) in
            
            if response.statusCode == 200{
                print("success in like")
            }
            else{
                print("error in like")
            }
        }, { (ErrorResponse) in
            print("error in like")
        })
        
        
    }
    @IBAction func enterAuction(_ sender: Any) {
        let vc = AuctionViewController.instantiate(storyboardName: "Auction")
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func bckBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension ContestDetailViewController {
    
    override func viewDidLayoutSubviews() {
        imagePageController.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        }
    }
}
