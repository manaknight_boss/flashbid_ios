//
//  ResultsViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 28/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import RxFirebase
import RxSwift
import LUExpandableTableView

class ResultsViewController: BaseViewController {
    
    var result = [Result]()
    
    @IBOutlet weak var tableView: LUExpandableTableView!
    
    private let ref = Database.database().reference()
    private let sectionHeaderReuseIdentifier = "MySectionHeader"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFirebaseForResults()
        
        self.tableView.register(UINib(nibName: "ResultsExpandableTableViewSectionHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: sectionHeaderReuseIdentifier)

        self.tableView.expandableTableViewDataSource = self
        self.tableView.expandableTableViewDelegate = self
        
        self.addTitleLogo()

        self.tableView!.tableFooterView = UIView()
    }
    
    private func setupFirebaseForResults() {
         var stopLoading = true
        startLoading()
        resultViewModel.listenForResult()
            .do(onNext: { result in
                if let oldResult = self.result.firstIndex(where: { $0.id == result.id }) {
                    Result.copy(from: result, to: &self.result[oldResult])
                } else {
                    self.result.append(result)
                }
            })
            .debounce(1, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { result in
                self.tableView.reloadData()
                if stopLoading {
                    self.stopLoading()
                    stopLoading = false
                }
            })
            .disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(sideBarItemClicked(notification:)), name: .sideBarItemClicked, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .sideBarItemClicked, object: nil)
    }
    
    @objc func sideBarItemClicked(notification: Notification) {
        self.sideBarItemClicked1(notification: notification)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sectionTitle(type:ContestType) -> String{
        if type == .LOTTERY {
            return "Sorteo"
        }
        else if type == .AUCTION {
            return "Subasta"
        }
        else if type == .SPECIAL_AUCTION{
            return "Subasta Flash"
        }
        else {
            return "Trivialidades"
        }
    }
    
}

// MARK: - LUExpandableTableViewDataSource

extension ResultsViewController: LUExpandableTableViewDataSource {
    func numberOfSections(in expandableTableView: LUExpandableTableView) -> Int {
        return self.result.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return self.result[section].winners.count
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "resultCell") as? ResultsTableViewCell else {
            assertionFailure("Cell shouldn't be nil")
            return UITableViewCell()
        }

        let winnersArray = self.result[indexPath.section].winners
        let winnerSet = winnersArray[indexPath.row]
        
        cell.nameLbl.text = winnerSet.firstName + " " + "\(winnerSet.lastName.first ?? " ").".uppercased()
        cell.photoImageView.kf.indicatorType = .activity
        cell.photoImageView.kf.setImage(with: URL(string: winnerSet.profileImage))
        if winnerSet.time > 0 {
            cell.timeLbl.text = "\(winnerSet.time)sec"
        }
        else{
            cell.timeLbl.text = ""
        }
        cell.msgLbl.text = winnerSet.message

        return cell
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
        guard let sectionHeader = expandableTableView.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderReuseIdentifier) as? ResultsExpandableTableViewSectionHeader else {
            assertionFailure("Section header shouldn't be nil")
            return LUExpandableTableViewSectionHeader()
        }
        
        let resultSet = self.result[section]
        
        sectionHeader.expandCollapseButton.isHidden = resultSet.winners.count == 0  ? true : false
        sectionHeader.dateLbl.text = resultSet.updatedAt
        sectionHeader.tag = resultSet.id
        
        // if type NOT EQUAL TO Trivia, download image
        if resultSet.type != .TRIVIALITIES{
            sectionHeader.myImageView.kf.indicatorType = .activity
            sectionHeader.myImageView.kf.setImage(with: URL(string: resultSet.image))
        }
        else{
            sectionHeader.myImageView.image = UIImage.init(named: "trivialidades")
        }
        
        sectionHeader.titleLbl.text = sectionTitle(type: resultSet.type!)
        sectionHeader.detailLbl.text = resultSet.message
        return sectionHeader
    }
}

// MARK: - LUExpandableTableViewDelegate

extension ResultsViewController: LUExpandableTableViewDelegate {
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 68
    }
    
    // MARK: - Optional
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, didSelectSectionHeader sectionHeader: LUExpandableTableViewSectionHeader, atSection section: Int) {
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func expandableTableView(_ expandableTableView: LUExpandableTableView, willDisplaySectionHeader sectionHeader: LUExpandableTableViewSectionHeader, forSection section: Int) {
    }
}

