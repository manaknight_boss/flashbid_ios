//
//  TermsAndConditionViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import RxFirebase
import Firebase
import RxSwift

class TermsAndConditionViewController: UIViewController, Storyboarded {

    @IBOutlet weak var termsAndConditionTitle: UILabel!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var okButton: UIButton!
    
    let loading = Loading()
    
    // Used to auto dispose the rx subscription
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpFont()
        
        loading.create(into: view)
        
        let ref = Database.database().reference()
        loading.start()
        ref.child(FirebaseNode.TERMS_AND_CONDITIONS)
            .rx
            .observeSingleEvent(.value)
            .subscribe(onNext: { [unowned self] snapshot in
                self.loading.stop()
                self.setTermsAndCondition(snapshot: snapshot)
            }).disposed(by: disposeBag)
    }
    
    private func setTermsAndCondition(snapshot: DataSnapshot) {
        
        snapshot
            .dictIterator()
            .forEach({
                termsTextView.text = $0?[FirebaseValue.DESCRIPTION] as? String
            })
    }
    
    private func setUpFont() {
        setRegularFont([termsAndConditionTitle, okButton], 15, .white)
        setRegularFont([termsTextView], 15, .black)
    }
    
    @IBAction func onOk(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
