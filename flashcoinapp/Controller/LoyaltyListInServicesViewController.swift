//
//  LoyaltyListInServicesViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class LoyaltyListInServicesViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    let cellReuseIdentifier = "loyaltyCell"
    var loyaltyData = [LoyaltyData]()
    var screenTitle = ""

    @IBOutlet weak var loyaltyTableView: UITableView!
    
    @IBOutlet weak var noDataLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loyaltyTableView.tableFooterView = UIView(frame: .zero)
        
        self.navigationItem.title = self.screenTitle
        
        self.fetchLoyaltyListService()
        // Do any additional setup after loading the view.
    }
    
    func fetchLoyaltyListService() {
        
        startLoading()

        loyalityListViewModel.getLoyalityList(accessToken: accessToken, { (loyaltyList) in
            self.loyaltyData = loyaltyList.items
            self.stopLoading()
            if self.loyaltyData.count > 0{
                self.loyaltyTableView.backgroundView = nil
                self.loyaltyTableView.reloadData()
            }
            else{
                self.loyaltyTableView.backgroundView = self.noDataLbl
            }
            
        }, errorHandler)
        
        
    }
    
    // MARK: - TableView Methods
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.loyaltyData.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
            as! LoyaltyListInAppsTableViewCell

        let loyaltyItem = self.loyaltyData[indexPath.row]

        cell.rowTitleLbl.text = loyaltyItem.companyName
        cell.rowImageLbl.text = "\(loyaltyItem.companyName.first ?? " ")".uppercased()
        cell.countLbl.text = "\(loyaltyItem.points)"
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}
