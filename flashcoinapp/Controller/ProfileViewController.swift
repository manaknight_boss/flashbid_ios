//
//  ProfileViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var updateButton: UIBarButtonItem!

    @IBOutlet weak var profilePhotoBtn: UIButton!
    @IBOutlet weak var editProfileButton: UIButton!
    
    @IBOutlet weak var personalInfoLabel: UILabel!
    @IBOutlet weak var personalInfoStackView: UIStackView!
    @IBOutlet weak var emailTextFiled: ProfileTextField!
    @IBOutlet weak var passwordTextFiled: ProfileTextField!
    @IBOutlet weak var firstNameTextFiled: ProfileTextField!
    @IBOutlet weak var lastNameTextFiled: ProfileTextField!
    @IBOutlet weak var phoneTextFiled: ProfileTextField!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var addressTextFiled: ProfileTextField!
    @IBOutlet weak var stateTextFiled: ProfileTextField!
    @IBOutlet weak var cityTextFiled: ProfileTextField!
    @IBOutlet weak var countryTextFiled: ProfileTextField!
    @IBOutlet weak var postalCodeTextFiled: ProfileTextField!
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var genderTableView: UITableView!
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var ageTableView: UITableView!
    
    var screenTitle = ""
    let genderHandler = GenderHandler()
    let ageGroupHandler = AgeGroupHandler()
    var countryDataSource = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.screenTitle
        
        profileViewModel.image
            .asObservable()
            .subscribe(onNext: { image in
                    self.profilePhotoBtn.setImage(image, for: .normal)
            })
            .disposed(by: disposeBag)
        
        genderTableView.delegate = genderHandler
        genderTableView.dataSource = genderHandler
        
        ageTableView.delegate = ageGroupHandler
        ageTableView.dataSource = ageGroupHandler
        
        setFonts()
        startLoading()
        profileViewModel.profileData
            .asObservable()
            .subscribe(onNext: { profileData in
                guard let profileData = profileData else { return }
                self.fillUIFromResponse(profileData)
                self.stopLoading()
            })
            .disposed(by: disposeBag)
    }
    
    private func setFonts() {
        setMediumFont([updateButton, personalInfoLabel], 16, .black)
        setRegularFont([editProfileButton], 16, .c48b2d2)
        setMediumFont([personalInfoLabel, addressLabel, genderLabel, ageLabel], 16, .black)
    }
    
    private func addCountryInPicker(country: String) {
        countryDataSource.append(country)
    }
    
    private func fillUIFromResponse(_ profileData: ProfileResponse) {
        firstNameTextFiled.text = profileData.firstName
        lastNameTextFiled.text = profileData.lastName
        emailTextFiled.text = profileData.email
        stateTextFiled.text = profileData.state
        countryTextFiled.text = profileData.country
        addCountryInPicker(country: profileData.country)
        cityTextFiled.text = profileData.city
        postalCodeTextFiled.text = profileData.postalCode
        addressTextFiled.text = profileData.address
        phoneTextFiled.text = profileData.phoneNumber
        if let data = ProfileDataResponse.decode(from: profileData.data) {
            genderHandler.selectGender(gender: data.gender)
            genderTableView.reloadData()
            ageGroupHandler.selectAgeGroup(index: data.ageGroup)
            ageTableView.reloadData()
        }
    }
    
    private func pickProfileImage() {
        if connected() {
            ImagePickerManager().pickImage(self, cameraTitle: LocalizedString.CAMERA, galleryTitle: LocalizedString.GALLERY) { image in
                if let data = image.jpeg(.medium), let compressedImage = UIImage(data: data) {
                    self.uploadImageToServer(image: compressedImage)
                } else {
                    self.uploadImageToServer(image: image)
                }
            }
        }
    }
    
    private func uploadImageToServer(image: UIImage) {
        startLoading()
        let shortenedImage = image.resizeImage(targetSize: CGSize.init(width: 250, height: 250))
        profileViewModel.uploadProfileImage(accessToken: accessToken, image: shortenedImage, { [unowned self] () in
            self.stopLoading()
        }, errorHandler)
    }
    
    // MARK: UI Action
    @IBAction func onOuterClick(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func onUpdate(_ sender: Any) {
        
        let email = emailTextFiled.text ?? ""
        let firstName = firstNameTextFiled.text ?? ""
        let lastName = lastNameTextFiled.text ?? ""
        let password = passwordTextFiled.text
        let state = stateTextFiled.text ?? ""
        let country = countryTextFiled.text ?? ""
        let city = cityTextFiled.text ?? ""
        let postalCode = postalCodeTextFiled.text ?? ""
        let address = addressTextFiled.text ?? ""
        let phoneNumber = phoneTextFiled.text ?? ""
        let gender = genderHandler.getGender()
        let ageGroup = ageGroupHandler.getAgeGroup()
        
        if email.isEmpty, email.isNotValidEmail() {
            emailTextFiled.showError(message: LocalizedString.EMAIL_VALIDATION)
            return
        }
        if firstName.isEmpty {
            firstNameTextFiled.showError(message: LocalizedString.FIRST_NAME_VALIDATION)
            return
        }
        if lastName.isEmpty {
            lastNameTextFiled.showError(message: LocalizedString.LAST_NAME_VALIDATION)
            return
        }
        if let password = password, password.isNotEmpty, password.isNotValidPassword() {
            passwordTextFiled.showError(message: LocalizedString.PASSWORD_VALIDATION)
            return
        }
        if phoneNumber.isEmpty {
            phoneTextFiled.showError(message: LocalizedString.PHONE_VALIDATION)
            return
        }
        
        if address.isEmpty {
            addressTextFiled.showError(message: LocalizedString.ADDRESS_VALIDATION)
            return
        }
        if state.isEmpty {
            stateTextFiled.showError(message: LocalizedString.STATE_VALIDATION)
            return
        }
        if city.isEmpty {
            cityTextFiled.showError(message: LocalizedString.CITY_VALIDATION)
            return
        }
        if postalCode.isEmpty {
            postalCodeTextFiled.showError(message: LocalizedString.POSTAL_CODE_VALIDATION)
            return
        }
        
        let request = ActionProfileEditRequest(email: email, password: password, firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, address: address, state: state, city: city, country: country, postalCode: postalCode, gender: gender, ageGroup: ageGroup)
        startLoading()
        profileViewModel.editProfile(accessToken: accessToken, request: request, { () in
            self.stopLoading()

            let alert = UIAlertController(title: LocalizedString.PROFILE_UPDATE_DIALOG_TITLE, message: LocalizedString.MESSAGE_PROFILE_UPDATED, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction.init(title: LocalizedString.OK, style: .default, handler: { (UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }, errorHandler)
    }
    
    @IBAction func profilePhotoBtnClicked(_ sender: Any) {
        pickProfileImage()
    }
    
    @IBAction func onEditProfile(_ sender: Any) {
        pickProfileImage()
    }
    
    @IBAction func onCountryClick(_ sender: Any) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: self.view.frame.width - 16, height: self.view.frame.width / 2)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 16, height: self.view.frame.width / 2))
        pickerView.delegate = self
        pickerView.dataSource = self
        vc.view.addSubview(pickerView)
        let editRadiusAlert = UIAlertController(title: "Choose Country", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        editRadiusAlert.setValue(vc, forKey: "contentViewController")
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: LocalizedString.CANCEL, style: .cancel, handler: nil))
        self.present(editRadiusAlert, animated: true)
    }
}

extension ProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        countryTextFiled.text = countryDataSource[row]
    }
}
