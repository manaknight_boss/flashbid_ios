//
//  QRScannerViewController.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//
import AVFoundation
import UIKit

class QRScannerViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var selectedScanOption = 0
    var selectedImage: UIImage?

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = LocalizedString.SCAN
        if selectedScanOption == 1{
            self.startCameraForQRScan()
        }
        else{
            self.detectQRFromImage()
        }
    }
    
    func detectQRFromImage(){
        if let qrcodeImg = selectedImage {
            let detector:CIDetector=CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])!
            let ciImage:CIImage=CIImage(image:qrcodeImg)!
            var qrCode=""
            
            let features=detector.features(in: ciImage)
            for feature in features as! [CIQRCodeFeature] {
                qrCode += feature.messageString!
            }
            if qrCode=="" {
                print("nothing scanned")
                showErrorMsgAndGoBack(LocalizedString.INVALID_CODE)
            }else{
                print("message: \(qrCode)")
                qrCodeStringFound(codeString: qrCode)
            }
        }
        else{
            showErrorMsgAndGoBack(LocalizedString.SOMETHING_WENT_WRONG)
        }
    }
    
    func startCameraForQRScan(){
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: LocalizedString.OK, style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false && selectedScanOption == 1) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            qrCodeStringFound(codeString: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func qrCodeStringFound(codeString: String) {

        var code = "", type = ""
        
        let decodedQR = codeString.base32DecodedString()
        print(decodedQR ?? "defaultQR")
        
        if decodedQR != nil && decodedQR!.contains(":"){
            code = decodedQR!.components(separatedBy: ":").first!
            type = decodedQR!.components(separatedBy: ":").last!.lowercased()
        }
        else {
            self.showErrorMsgAndGoBack(LocalizedString.INVALID_CODE)
            return
        }
        
        if code.isEmpty || type.isEmpty {
            showErrorMsgAndGoBack(LocalizedString.SOMETHING_WENT_WRONG)
            return
        }

        let request = ActionQRScanRequest.init(code: code, type: type)
        startLoading()
        qrScanViewModel.sendScannedData(accessToken: accessToken, request: request, { (response) in
            self.stopLoading()
            
            // push to User OTP screen (user)
            if type == Constants.QRType.user{
                let vcName = UIStoryboard(name: Constants.Storyboard.Otp, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.UserOtpViewController) as! UserOtpViewController
                vcName.user = response
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            // push to Sticker OTP screen (sticker)
            else if type == Constants.QRType.sticker{
                let vcName = UIStoryboard(name: Constants.Storyboard.Otp, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.StickerOtpViewController) as! StickerOtpViewController
                vcName.stickerCode = decodedQR ?? ""
                vcName.stickerVendorId = response.vendorId ?? 0
                self.navigationController?.pushViewController(vcName, animated: true)
            }
            // push to Pay OTP screen (pay)
            else if type == Constants.QRType.pay{
                let vcName = UIStoryboard(name: Constants.Storyboard.Otp, bundle: nil).instantiateViewController(withIdentifier: Constants.Controller.PayOtpViewController) as! PayOtpViewController
                if response.id != nil && response.id ?? 0 > 0{
                    vcName.id = response.id ?? 0
                    self.navigationController?.pushViewController(vcName, animated: true)
                }
                else{
                    self.showErrorMsgAndGoBack(LocalizedString.SOMETHING_WENT_WRONG)
                }
            }
            // push to Contest Detail screen (fotp)
            else if type == Constants.QRType.fotp || type == Constants.QRType.lottery || type == Constants.QRType.auction || type == Constants.QRType.otp{
                let vcName = ContestDetailViewController.instantiate(storyboardName: "Contest")
                vcName.contest = Contest(
                    firebaseKey: nil,
                    roomId: response.roomId ?? 0,
                    data: nil,
                    finalBid: response.finalBid ?? 0,
                    secondaryImage: response.secondaryImage ?? "",
                    createdAt: response.createdAt ?? "",
                    description: response.description ?? "",
                    winnerId: 0,
                    initialBid: response.initialBid ?? 0,
                    title: response.title ?? "",
                    type: ContestType(response.type ?? ""),
                    bidAmount: response.bidAmount ?? 0,
                    updatedAt: response.updatedAt ?? "",
                    qrCode: "",
                    id: response.id!,
                    stock: response.stock ?? 0,
                    delivery: response.delivery ?? "",
                    image: response.image ?? "",
                    marketValue: "",
                    ticketSold: response.ticketSold ?? 0,
                    size: "",
                    buyAtAmount: response.buyAtAmount ?? 0,
                    ticketAmount: response.ticketAmount ?? 0,
                    ticketTotal: response.ticketTotal ?? 0,
                    itemWorth: response.itemWorth ?? 0,
                    status: ContentStatus(response.status ?? "")
                )
                self.navigationController?.pushViewController(vcName, animated: true)

            }
                
            else{
                let alert = UIAlertController.init(title: "QR Response Success", message: decodedQR, preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction.init(title: LocalizedString.OK, style: .default, handler: { (UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }))
            }

        }, { (ErrorResponse) in
            self.errorHandler(error: ErrorResponse)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.navigationController?.popViewController(animated: true)
            }
        })

    }
    
    func showErrorMsgAndGoBack(_ msg: String){
        showErrorDialog(message: msg)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    
}
