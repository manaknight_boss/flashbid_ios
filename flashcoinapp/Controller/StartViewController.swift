//
//  ViewController.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 08/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import RxKeyboard
import RxSwift
import FacebookCore
import FacebookLogin
import Firebase

class StartViewController: UIViewController, Storyboarded {
    

    @IBOutlet weak var flashBidLabel: UILabel!
    @IBOutlet weak var lightiningLabel: UILabel!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var termsAndConditionLabel: UILabel!
    
    //  Login Dialog
    @IBOutlet weak var loginCardView: CardView!
    @IBOutlet weak var loginCardBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginEmailTextField: CustomTextField!
    @IBOutlet weak var loginPasswordTextField: CustomTextField!
    @IBOutlet weak var loginUpdatePasswordButton: UIButton!
    @IBOutlet weak var loginForgotPasswordButton: UIButton!
    @IBOutlet weak var loginLoginButton: UIButton!
    
    // Register Dialog
    @IBOutlet weak var registerCardView: CardView!
    @IBOutlet weak var registerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var registerEmailTextField: CustomTextField!
    @IBOutlet weak var registerPasswordTextField: CustomTextField!
    @IBOutlet weak var registerFirstNameTextField: CustomTextField!
    @IBOutlet weak var registerSurnameTextField: CustomTextField!
    @IBOutlet weak var registerPhoneTextField: CustomTextField!
    @IBOutlet weak var registerRegisterButton: UIButton!
    @IBOutlet weak var registerTermsAndConditionLabel: UILabel!
    
    // Forgot Password Dialog
    @IBOutlet weak var forgotPasswordCardView: CardView!
    @IBOutlet weak var forgotPasswordBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var forgotPasswordEmailTextField: CustomTextField!
    @IBOutlet weak var forgotPasswordUpdatePasswordButton: UIButton!
    
    // Update Password Dialog
    @IBOutlet weak var updatePasswordCardView: CardView!
    @IBOutlet weak var updatePasswordCardBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var updatePasswordCodeTextField: CustomTextField!
    @IBOutlet weak var updatePasswordPasswordTextField: CustomTextField!
    @IBOutlet weak var updatePasswordUpdatePasswordButton: UIButton!
    
    // Used to up the UI when keyboard open
    @IBOutlet weak var safeAreaBottomHeightConstraint: NSLayoutConstraint!
    
    private let ANIMATION_DURATION_OF_KEYBOARD = 0.3
    private var keyboardHeight: CGFloat?
    
    // Card Show/Hide with Animation
    private var lastCardUp: CardView?
    private var cardConstraintDict: [CardView: NSLayoutConstraint] {
        get {
            return [
                loginCardView: loginCardBottomConstraint,
                registerCardView: registerBottomConstraint,
                forgotPasswordCardView: forgotPasswordBottomConstraint,
                updatePasswordCardView: updatePasswordCardBottomConstraint
            ]
        }
    }
    
    // Used to auto dispose the rx subscription
    let disposeBag = DisposeBag()
    
    let loading = Loading()
    var loginPassword = ""
    var registerPassword = ""
    var activeTextField: UITextField?
    let numberToolbar: UIToolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpFont()
        setTagAndDelegateOnEachCardTextFields()
        termsAndConditionLabel.underline(LocalizedString.THE_PRIVACY_POLICIES_AND_TERMS_OF_SERVICES)
        registerTermsAndConditionLabel.underline(LocalizedString.THE_PRIVACY_POLICIES_AND_TERMS_OF_SERVICES)
        loginUpdatePasswordButton.underline()
        loginForgotPasswordButton.underline()
        
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [unowned self] keyboardVisibleHeight in
                self.keyboardHeight = keyboardVisibleHeight
                self.upTheTextField()
            })
            .disposed(by: disposeBag)
        loading.create(into: view)
        
        let ref = Database.database().reference()
        ref.child(FirebaseNode.MAINTENANCE)
            .rx
            .observeEvent(.value)
            .subscribe({ [unowned self] valueEvent in
                if let element = valueEvent.element {
                    self.checkForMaintenance(snapshot: element)
                }
            }).disposed(by: disposeBag)
        
        numberToolbar.barStyle = UIBarStyle.blackTranslucent
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(StartViewController.endEditing))
        ]
        
        numberToolbar.sizeToFit()
        
        registerPhoneTextField.inputAccessoryView = numberToolbar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loginPassword = ""
        loginPasswordTextField.text = ""
    }
    
    @objc func endEditing() {
        self.view.endEditing(true)
    }
    
    private func upTheTextField() {
        if let keyboardVisibleHeight = keyboardHeight {
            if keyboardVisibleHeight == 0 {
                safeAreaBottomHeightConstraint.constant = 0
                
            } else {
                if let activeTextField = self.activeTextField {
                    self.keyboardHeight = keyboardVisibleHeight
                    let textFieldMaxY = self.view.convert(activeTextField.frame, from: activeTextField.superview).maxY + safeAreaBottomHeightConstraint.constant
                    let keyboardMinY = self.view.frame.height - keyboardVisibleHeight
                    safeAreaBottomHeightConstraint.constant = textFieldMaxY - keyboardMinY + 20
                }
            }
            UIView.animate(withDuration: self.ANIMATION_DURATION_OF_KEYBOARD) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func setUpFont() {
        
        // Start Screen
        setRegularFont([flashBidLabel], 40, .c1f87ae)
        setMediumFont([lightiningLabel], 16, .ce2922b)
        setLightFont([fbButton], 15, .white)
        setBoldFont([registerButton, loginButton], 15, .black)
        setLightFont([termsAndConditionLabel], 12, .c999999)
        
        // Login Dialog
        setLightFont([loginEmailTextField, loginPasswordTextField], 15, .c919090)
        setLightFont([loginUpdatePasswordButton, loginForgotPasswordButton], 11, .c919090)
        setLightFont([loginLoginButton], 15, .white)
        
        // Register Dialog
        setLightFont([registerEmailTextField, registerPasswordTextField, registerFirstNameTextField, registerSurnameTextField, registerPhoneTextField], 15, .c919090)
        setLightFont([registerRegisterButton], 15, .white)
        setLightFont([registerTermsAndConditionLabel], 12, .c999999)
        
        // Forgot Password Dialog
        setLightFont([forgotPasswordEmailTextField], 15, .c919090)
        setLightFont([forgotPasswordUpdatePasswordButton], 15, .white)
        
        // Update Password Dialog
        setLightFont([updatePasswordCodeTextField, updatePasswordPasswordTextField], 15, .c919090)
        setLightFont([updatePasswordUpdatePasswordButton], 15, .white)
    }
    
    private func setTagAndDelegateOnEachCardTextFields() {
        cardConstraintDict.forEach { (cardConstraint) in
            let (card, _) = cardConstraint
            card.setIncrementalTagOnEachTextField()
            card.setTextFieldDelegateOnEachTextField(delegate: self)
        }
    }
    
    private func animateUp(card: CardView) {
        lastCardUp = card
        let constraint = cardConstraintDict[card]!
        constraint.constant = card.frame.height
        UIView.animate(withDuration: ANIMATION_DURATION_OF_KEYBOARD) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func animateDown() {
        guard let card = lastCardUp else { return }
        let constraint = cardConstraintDict[card]!
        constraint.constant = 0
        UIView.animate(withDuration: ANIMATION_DURATION_OF_KEYBOARD) {
            self.view.layoutIfNeeded()
        }
        card.emptyChildTextFields()
    }
    
    private func showValidationError(textField: CustomTextField, message: String) {
        textField.becomeFirstResponder()
        textField.showError(message: message)
    }
    
    // MARK: Post API Actions
    private func handleLoggedIn(tokenResponse: TokenUserIdResponse) {
        animateDown()
        loginCardView.emptyChildTextFields()
        Login.saveLoginInformation(accessToken: tokenResponse.accessToken, userId: tokenResponse.userId)
        startCoordinator.pushToLandingTabBarController()
        
    }
    
    private func handleRegistered(tokenResponse: TokenUserIdResponse) {
        animateDown()
        registerCardView.emptyChildTextFields()
        Login.saveLoginInformation(accessToken: tokenResponse.accessToken, userId: tokenResponse.userId)
        showSuccessDialog(message: LocalizedString.REGISTER_SUCCESSFUL) { _ in
            self.startCoordinator.pushToLandingTabBarController()
        }
    }
    
    private func getBasicInfoFromFacebook() {
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                print("Facebook: Custom Graph Request Succeeded: \(response)")
                self.loginWithFacebook(response)
            case .failed(let error):
                print("Facebook: Custom Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    private func loginWithFacebook(_ response: MyProfileRequest.Response) {
        loading.start()
        let facebookRequest = FacebookLoginRequest(email: response.email, firstName: response.firstName, lastName: response.lastName, accessToken: AccessToken.current?.authenticationToken ?? "empty_fb_token")
        startViewModel.loginWithFacebook(firebaseToken: firebaseToken, request: facebookRequest, { (response) in
            self.loading.stop()
            self.handleLoggedIn(tokenResponse: response)
        }, { (error) in
            self.showErrorDialog(message: error.message ?? LocalizedString.SOMETHING_WENT_WRONG)
        })
    }
    
    // MARK: Common Actions
    @IBAction func onTermsAndCondition(_ sender: Any) {
        startCoordinator.openTermsAndCondition(this: self)
    }
    
    @IBAction func onOuterClick(_ sender: Any) {
        if keyboardHeight != 0 {
            self.view.endEditing(true)
        } else {
            animateDown()
        }
    }
    
    
    // MARK: Bottom View Actions
    @IBAction func onFacebookLogin(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
                
            case .failed(let error):
                printDebug("Facebook: User login error: \(error.localizedDescription)")
                // self.showErrorDialog(message: error.localizedDescription)
                
            case .cancelled:
                printDebug("Facebook: User cancelled login.")
                
            case .success(_, _, _):
                self.getBasicInfoFromFacebook()
            }
        }
    }
    
    @IBAction func onLogin(_ sender: Any) {
        animateUp(card: loginCardView)
    }
    
    @IBAction func onRegister(_ sender: Any) {
        animateUp(card: registerCardView)
    }
    
    
    // MARK: Login Card Actions
    @IBAction func loginOnUpdatePasssword(_ sender: Any) {
        animateDown()
        animateUp(card: updatePasswordCardView)
    }
    
    @IBAction func loginOnForgotPassword(_ sender: Any) {
        animateDown()
        animateUp(card: forgotPasswordCardView)
    }
    
    @IBAction func loginOnLogin(_ sender: Any) {
        self.view.endEditing(true)
        guard let email = loginEmailTextField.text, email.isValidEmail() else {
            showValidationError(textField: loginEmailTextField, message: LocalizedString.EMAIL_VALIDATION)
            return
        }
        let password = loginPassword
        guard password.isValidPassword() else {
            showValidationError(textField: loginPasswordTextField, message: LocalizedString.PASSWORD_VALIDATION)
            return
        }
        loading.start()
        startViewModel.login(firebaseToken: firebaseToken, request: LoginRequest(email: email, password: password),
            { [unowned self] (tokenResponse) in
                self.loading.stop()
                self.handleLoggedIn(tokenResponse: tokenResponse)
            },
            { [unowned self] (error) in
                self.loading.stop()
                self.showErrorDialog(message: error.message ?? LocalizedString.SOMETHING_WENT_WRONG)
            }
        )
    }
    
    
    // MARK: Register Card Actions
    @IBAction func registerOnRegister(_ sender: Any) {
        self.view.endEditing(true)
        guard let email = registerEmailTextField.text, email.isValidEmail() else {
            showValidationError(textField: registerEmailTextField, message: LocalizedString.EMAIL_VALIDATION)
            return
        }
        let password = registerPassword
        guard password.isValidPassword() else {
            showValidationError(textField: registerPasswordTextField, message: LocalizedString.PASSWORD_VALIDATION)
            return
        }
        guard let firstName = registerFirstNameTextField.text, firstName.isNotEmpty else {
            showValidationError(textField: registerFirstNameTextField, message: LocalizedString.FIRST_NAME_VALIDATION)
            return
        }
        guard let surName = registerSurnameTextField.text, surName.isNotEmpty else {
            showValidationError(textField: registerSurnameTextField, message: LocalizedString.LAST_NAME_VALIDATION)
            return
        }
        guard let phone = registerPhoneTextField.text, phone.isNotEmpty else {
            showValidationError(textField: registerPhoneTextField, message: LocalizedString.PHONE_VALIDATION)
            return
        }
        loading.start()
        startViewModel.register(firebaseToken: firebaseToken, request: RegisterRequest(email: email, password: password, firstName: firstName, surName: surName, phone: phone),
            { [unowned self] (tokenResponse) in
                self.loading.stop()
                self.handleRegistered(tokenResponse: tokenResponse)
            },
            { [unowned self] (error) in
                self.loading.stop()
                self.showErrorDialog(message: error.message ??  LocalizedString.SOMETHING_WENT_WRONG)
            }
        )
    }
    
    // MARK: Forgot Password Card Actions
    @IBAction func forgotPasswordOnUpdatePassword(_ sender: Any) {
        guard let email = forgotPasswordEmailTextField.text, email.isValidEmail() else {
            showValidationError(textField: forgotPasswordEmailTextField, message: LocalizedString.EMAIL_VALIDATION)
            return
        }
        loading.start()
        startViewModel.forgotPassword(request: ForgotPasswordRequest(email: email),
            { [unowned self] (messageResponse) in
                self.loading.stop()
                self.forgotPasswordCardView.emptyChildTextFields()
                self.showSuccessDialog(message: messageResponse.message) { _ in
                    self.animateDown()
                    self.animateUp(card: self.updatePasswordCardView)
                }
            },
            { [unowned self] (error) in
                self.loading.stop()
                self.showErrorDialog(message: error.message ??  LocalizedString.SOMETHING_WENT_WRONG)
            }
        )
    }
    
    
    // MARK: Update Password Card Actions
    @IBAction func updatePasswordOnUpdatePassword(_ sender: Any) {
        guard let code = updatePasswordCodeTextField.text, code.isNotEmpty else {
            showValidationError(textField: updatePasswordCodeTextField, message: LocalizedString.ENTER_VALID_CODE)
            return
        }
        guard let password = updatePasswordPasswordTextField.text, password.isValidPassword() else {
            showValidationError(textField: updatePasswordPasswordTextField, message: LocalizedString.PASSWORD_VALIDATION)
            return
        }
        loading.start()
        startViewModel.updatePassword(request: UpdatePasswordRequest(code: code, password: password),
            { [unowned self] (messageResponse) in
                self.loading.stop()
                self.updatePasswordCardView.emptyChildTextFields()
                self.showSuccessDialog(message: LocalizedString.MESSAGE_PASSWORD_RESET) { _ in
                    self.animateDown()
                    self.animateUp(card: self.loginCardView)
                }
            },
            { [unowned self] (error) in
                self.loading.stop()
                self.showErrorDialog(message: error.message ??  LocalizedString.SOMETHING_WENT_WRONG)
            }
        )
    }
}

extension StartViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        self.upTheTextField()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginPasswordTextField {
            if string.isNotEmpty {
                loginPassword += string
                if let text = textField.text {
                    textField.text = text + "*"
                } else {
                    textField.text = "*"
                }
                return false
            }
            let mutableSelf = NSMutableString(string: loginPassword)
            mutableSelf.deleteCharacters(in: range)
            loginPassword = String(mutableSelf)
        }
        if textField == registerPasswordTextField {
            if string.isNotEmpty {
                registerPassword += string
                if let text = textField.text {
                    textField.text = text + "*"
                } else {
                    textField.text = "*"
                }
                return false
            }
            let mutableSelf = NSMutableString(string: registerPassword)
            mutableSelf.deleteCharacters(in: range)
            registerPassword = String(mutableSelf)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
}

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        
        let email: String
        let firstName: String
        let lastName: String
        
        init(rawResponse: Any?) {
            printDebug(rawResponse)
            let dict = rawResponse as? [String: Any]
            email = dict?["email"] as? String ?? ""
            firstName = dict?["first_name"] as? String ?? ""
            lastName = dict?["last_name"] as? String ?? ""
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, first_name, email, last_name"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}
