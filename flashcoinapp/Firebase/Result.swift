//
//  Result.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 30/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Firebase

struct ResultWinner {
    
    var id: Int
    var firstName: String
    var lastName: String
    var profileImage: String
    var score: Int
    var total: Int
    var time: Int
    var message: String
    
    static func list(from array: [Any]) -> [ResultWinner] {
        var winners = [ResultWinner]()
        for element in array {
            if let dict = element as? [String: Any] {
                var winner = ResultWinner(
                    id: dict["id"] as? Int ?? 0,
                    firstName: dict["first_name"] as? String ?? "",
                    lastName: dict["last_name"] as? String ?? "",
                    profileImage: dict["profile_image"] as? String ?? "",
                    score: dict["score"] as? Int ?? 0,
                    total: dict["total"] as? Int ?? 0,
                    time: dict["time"] as? Int ?? 0,
                    message: ""
                )
                if winner.score == winner.total {
                    winner.message = LocalizedString.MESSAGE_PERFECT_SCORE
                } else {
                    winner.message = "Uff! Estuviste muy cerca! Tu Score es \(winner.score)/\(winner.total)"
                }
                winners.append(winner)
            }
        }
        return winners
    }
}

struct Result {
    var finalBid: Int
    var id: Int
    var image: String
    var name: String
    var profileImage: String
    var status: ContentStatus?
    var title: String
    var type: ContestType?
    var updatedAt: String
    var winners: [ResultWinner]
    var message: String
    
    static func newFromDataSnapshot(_ snapshot: DataSnapshot) -> Result {
        let dict = snapshot.value as! [String: Any]
        var result = Result(
            finalBid: dict["final_bid"] as? Int ?? 0,
            id: dict["id"] as! Int,
            image: dict["image"] as? String ?? "",
            name: dict["name"] as? String ?? "",
            profileImage: dict["profile_image"] as? String ?? "",
            status: ContentStatus(dict["status"] as? String ?? ""),
            title: dict["title"] as? String ?? "",
            type: ContestType(dict["type"] as? String ?? ""),
            updatedAt: dict["updated_at"] as? String ?? "",
            winners: [],
            message: ""
        )
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        if let updateAtDate = dateFormater.date(from: result.updatedAt) {
            dateFormater.dateFormat = "MMM dd yyyy"
            result.updatedAt = dateFormater.string(from: updateAtDate)
        }
        
        if result.status == .LOST {
            result.message = LocalizedString.MESSAGE_NO_WINNER
        } else {
            result.message = result.title
            if let type = result.type, type == .TRIVIALITIES {
                result.winners = ResultWinner.list(from: dict["winners"] as? [Any] ?? [])
            } else {
                
                var firstName = ""
                var lastName = ""
                if result.name.isNotEmpty {
                    firstName = String(result.name.split(separator: " ").first ?? "")
                    lastName = String(result.name.split(separator: " ").last ?? "")
                    if lastName.isNotEmpty {
                        lastName = String(lastName.prefix(1)) + "."
                    }
                }
                
                var winner = ResultWinner(id: 0, firstName: firstName, lastName: lastName, profileImage: result.profileImage, score: 0, total: 0, time: 0, message: "")
                if let type = result.type, type == .LOTTERY {
                    winner.message = LocalizedString.MESSAGE_CONGRATULATION_TO_WINNER
                } else {
                    winner.message = "Felicidades al postor mas alto con una oferta de \(result.finalBid) FP."
                }
                result.winners.append(winner)
            }
        }
        return result
    }
    
    static func copy(from: Result, to: inout Result) {
        to.finalBid = from.finalBid
        to.id = from.id
        to.image = from.image
        to.name = from.name
        to.profileImage = from.profileImage
        to.status = from.status
        to.title = from.title
        to.type = from.type
        to.updatedAt = from.updatedAt
        to.winners = from.winners
        to.message = from.message
    }
}
