//
//  ContestType.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 30/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

enum ContestType: String {
    
    case SPECIAL_AUCTION = "a"
    case LOTTERY = "l"
    case AUCTION = "ta"
    case TRIVIALITIES = "t"
    case FOTP = "o"
    case OTP = "r"
    
    init?(_ string: String) {
        self.init(rawValue: string.lowercased())
    }
}

struct ItemType {
    
    static var SPECIAL_AUCTION = "a"
    static var LOTTERY = "l"
    static var AUCTION = "ta"
    static var TRIVIALITIES = "t"
    static var FOTP = "o"
    static var OTP = "r"
}
