//
//  FirebaseConstant.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct FirebaseNode {
    
    static let TERMS_AND_CONDITIONS = "terms"
    static let MAINTENANCE = "maintenance"
    static let USER = "user"
    static let ALL_ITEMS = "allItems"
    static let RESULTS = "results"
    static let RESULTS_FIRST_CHILD = "1"
    static let CONTESTS = "1"
}

struct FirebaseValue {
    
    static let DESCRIPTION = "description"
}
