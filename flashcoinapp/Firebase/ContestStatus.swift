//
//  ContestStatus.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 30/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

enum ContentStatus: String {
    
    case ACTIVE = "active"
    case CLAIMED = "claimed"
    case LOST = "lost"
    case COMPLETE = "complete"
    case QUEUE = "queue"
    case FLASH = "flash"
    
    init?(_ string: String) {
        self.init(rawValue: string.lowercased())
    }
}

struct ItemStatus {
    static var ACTIVE = "active"
    static var CLAIMED = "claimed"
    static var LOST = "lost"
    static var COMPLETE = "complete"
    static var QUEUE = "queue"
    static var FLASH = "flash"
}
