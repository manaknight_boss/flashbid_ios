//
//  Terms.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

struct Terms: Decodable {
    
    var description: String?
    var id: Int?
}
