//
//  ContestData.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 01/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

struct ContentData {
    
    var startTime: Int64?
    
    init(data: [String: Any]?) {
        if let data = data {
            startTime = data["start"] as? Int64
        }
    }
}
