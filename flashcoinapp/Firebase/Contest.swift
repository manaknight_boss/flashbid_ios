//
//  Contests.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 28/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Firebase

struct Contest {
    
    var firebaseKey: String?
    var roomId: Int
    var data: ContentData?
    var finalBid: Int
    var secondaryImage: String
    var createdAt: String
    var description: String
    var winnerId: Int
    var initialBid: Int
    var title: String
    var type: ContestType?
    var bidAmount: Int
    var updatedAt: String
    var qrCode: String
    var id: Int
    var stock: Int
    var delivery: String
    var image: String
    var marketValue: String
    var ticketSold: Int
    var size: String
    var buyAtAmount: Int
    var ticketAmount: Int
    var ticketTotal: Int
    var itemWorth: Int
    var status: ContentStatus?
    
    static func newFromDataSnapshot(_ snapshot: DataSnapshot) -> Contest {
        let dict = snapshot.value as! [String: Any]
        return Contest(
            firebaseKey: snapshot.key,
            roomId: dict["room_id"] as? Int ?? 0,
            data: ContentData(data: dict["data"] as? [String: Any]),
            finalBid: dict["final_bid"] as? Int ?? 0,
            secondaryImage: dict["secondary_image"] as? String ?? "",
            createdAt: dict["created_at"] as? String ?? "",
            description: dict["description"] as? String ?? "",
            winnerId: dict["winner_id"] as? Int ?? 0,
            initialBid: dict["initial_bid"] as? Int ?? 0,
            title: dict["title"] as? String ?? "",
            type: ContestType(dict["type"] as? String ?? ""),
            bidAmount: dict["bid_amount"] as? Int ?? 0,
            updatedAt: dict["updated_at"] as? String ?? "",
            qrCode: dict["qr_code"] as? String ?? "",
            id: dict["id"] as? Int ?? 0,
            stock: dict["stock"] as? Int ?? 0,
            delivery: dict["delivery"] as? String ?? "",
            image: dict["image"] as? String ?? "",
            marketValue: dict["market_value"] as? String ?? "",
            ticketSold: dict["ticket_sold"] as? Int ?? 0,
            size: dict["size"] as? String ?? "",
            buyAtAmount: dict["buy_at_amount"] as? Int ?? 0,
            ticketAmount: dict["ticket_amount"] as? Int ?? 0,
            ticketTotal: dict["ticket_total"] as? Int ?? 0,
            itemWorth: dict["item_worth"] as? Int ?? 0,
            status: ContentStatus(dict["status"] as? String ?? "")
        )
    }
    
    static func listFromDataSnapshot(_ snapshot: DataSnapshot) -> [Contest] {
        var list = [Contest]()
        for child in snapshot.children {
            let contest = newFromDataSnapshot(child as! DataSnapshot)
            list.append(contest)
        }
        return list
    }
    
    static func copy(from: Contest, to: inout Contest) {
        to.roomId = from.roomId
        to.data = from.data
        to.finalBid = from.finalBid
        to.secondaryImage = from.secondaryImage
        to.createdAt = from.createdAt
        to.description = from.description
        to.winnerId = from.winnerId
        to.initialBid = from.initialBid
        to.title = from.title
        to.type  = from.type
        to.bidAmount = from.bidAmount
        to.updatedAt = from.updatedAt
        to.qrCode = from.qrCode
        to.id = from.id
        to.stock = from.stock
        to.delivery = from.delivery
        to.image = from.image
        to.marketValue = from.marketValue
        to.ticketSold = from.ticketSold
        to.size = from.size
        to.buyAtAmount = from.buyAtAmount
        to.ticketAmount = from.ticketAmount
        to.ticketTotal = from.ticketTotal
        to.itemWorth = from.itemWorth
        to.status =   from.status
    }
}
