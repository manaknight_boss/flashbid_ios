//
//  Login+CoreDataClass.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Login)
public class Login: NSManagedObject {
    
    // MARK: Helper
    class func save(consumer: (_ login: Login) -> Void) {
        let fetchRequest: NSFetchRequest<Login> = Login.fetchRequest()
        do {
            let loginList = try CoreDataHelper.context.fetch(fetchRequest)
            var login: Login? = loginList.first
            if login == nil {
                login = Login(context: CoreDataHelper.context)
            }
            consumer(login!)
            CoreDataHelper.saveContext()
        } catch {
            printDebug("Core Data reading error: \(error)")
        }
    }
    
    class func read(consumer: (_ login: Login) -> Void) {
        let fetchRequest: NSFetchRequest<Login> = Login.fetchRequest()
        do {
            let login = try CoreDataHelper.context.fetch(fetchRequest)
            if let loginData = login.first {
                consumer(loginData)
            }
        } catch {
            printDebug("Core Data reading error: \(error)")
        }
    }
    
    // MARK: Save
    class func saveLoginInformation(accessToken: String?, userId: Int) {
        save { (login) in
            login.accessToken = accessToken
            login.userId = Int64(userId)
        }
    }
    
    class func saveFirebaseToken(firebaseToken: String) {
        save { (login) in
            login.firebaseToken = firebaseToken
        }
    }
    
    // MARK: READ
    class func getAccessToken() -> String? {
        var returnValue: String? = nil
        read { (login) in
            
            returnValue = login.accessToken
            
        }
        return returnValue
    }
    
    class func getFirebaseToken() -> String? {
        var returnValue: String? = nil
        read { (login) in
            
            returnValue = login.firebaseToken
            
        }
        return returnValue
    }
    
    class func getUserId() -> Int {
        var returnValue: Int = 0
        read { (login) in
            returnValue = Int(login.userId)
        }
        return returnValue
    }
    
    class func getUserIdString() -> String {
        return String(getUserId())
    }
}

