//
//  Login+CoreDataProperties.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//
//

import Foundation
import CoreData


extension Login {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Login> {
        return NSFetchRequest<Login>(entityName: "Login")
    }

    @NSManaged public var accessToken: String?
    @NSManaged public var firebaseToken: String?
    @NSManaged public var userId: Int64

}
