//
//  Loading.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import MaterialComponents.MaterialActivityIndicator

class Loading: MDCActivityIndicator {
    
    func create(into view: UIView) {
        let activityIndicator = self
        activityIndicator.sizeToFit()
        activityIndicator.center = view.center
        activityIndicator.radius = 24
        activityIndicator.strokeWidth = 4
        activityIndicator.cycleColors = [ .c1f87ae ]
        view.addSubview(activityIndicator)
    }
    
    func start() {
        self.startAnimating()
    }
    
    func stop() {
        self.stopAnimating()
    }
}
