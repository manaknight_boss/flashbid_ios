//
//  ContestProgressBar.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 28/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

@IBDesignable class ContestProgressBar: UIView {
    
    @IBInspectable var bgColor: UIColor = UIColor.green
    @IBInspectable var progressColor: UIColor = UIColor.white
    
    var bgPath: UIBezierPath!
    var progressPath: UIBezierPath!
    var shapeLayer: CAShapeLayer!
    var progressLayer: CAShapeLayer!
    
    let padding: CGFloat = 1.0
    
    var progress: Float = 5 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        bgPath = UIBezierPath()
        progressPath = UIBezierPath()
        self.simpleShape()
    }
    
    func simpleShape() {
        
        createLinearPath()
        createProgressPath()
        
        shapeLayer = CAShapeLayer()
        shapeLayer.path = bgPath.cgPath
        shapeLayer.lineWidth = bounds.height
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = bgColor.cgColor
        
        progressLayer = CAShapeLayer()
        progressLayer.path = progressPath.cgPath
        progressLayer.lineWidth = bounds.height - (padding * 2)
        progressLayer.fillColor = nil
        progressLayer.strokeColor = progressColor.cgColor
        progressLayer.strokeEnd = CGFloat(min(progress, 100) / 100)
        
        let px = bounds.minX + padding
        let py = bounds.minY + padding
        let tpw = bounds.width - (padding * 2)
        let pw = tpw * progressLayer.strokeEnd
        let ph = bounds.height - (padding * 2)
        let pc = (bounds.height - (padding * 2)) / 2
        let progressBounds = CGRect(x: px, y: py, width: pw, height: ph)
        let progressPathWithRadius = UIBezierPath(roundedRect: progressBounds, cornerRadius: pc)
        let progressMaskLayer = CAShapeLayer()
        progressMaskLayer.path = progressPathWithRadius.cgPath
        progressLayer.mask = progressMaskLayer
        
        let pathWithRadius = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2)
        let maskLayer = CAShapeLayer()
        maskLayer.path = pathWithRadius.cgPath
        self.layer.mask = maskLayer
        
        self.layer.addSublayer(shapeLayer)
        self.layer.addSublayer(progressLayer)
    }
    
    private func createLinearPath() {
        let centerY = self.frame.height / 2
        bgPath.move(to: CGPoint(x: 0, y: centerY))
        bgPath.addLine(to: CGPoint(x: self.frame.width, y: centerY))
        bgPath.close()
    }
    
    private func createProgressPath() {
        let centerY = self.frame.height / 2
        progressPath.move(to: CGPoint(x: padding, y: centerY))
        progressPath.addLine(to: CGPoint(x: self.frame.width - padding, y: centerY))
        progressPath.close()
    }
}
