//
//  CustomTextField.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

@IBDesignable class CustomTextField: ValidationTextField {

    @IBInspectable var inset: CGFloat = 7

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: inset)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
}
