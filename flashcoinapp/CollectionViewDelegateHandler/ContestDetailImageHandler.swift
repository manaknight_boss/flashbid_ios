//
//  ContestDetailImageHandler.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit
import Kingfisher

class ContestDetailImageHandler: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var images = [String]() {
        didSet {
            imagePageController.numberOfPages = images.count
            notifier()
        }
    }
    var notifier: (() -> Void)!
    var imagePageController: UIPageControl! {
        didSet {
            imagePageController.currentPage = 0
            imagePageController.pageIndicatorTintColor = .cc0b8b8
            imagePageController.currentPageIndicatorTintColor = .white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContestDetailImageCell.className, for: indexPath) as! ContestDetailImageCell
        cell.imageUrl = URL(string: images[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        imagePageController.currentPage = Int(pageNumber)
    }
}

class ContestDetailImageCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    
    var imageUrl: URL! {
        didSet {
            imageView.setImage(imageUrl)
        }
    }
}
