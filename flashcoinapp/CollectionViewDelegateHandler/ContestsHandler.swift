//
//  ContestsHandler.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 28/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import Kingfisher

class ContestsHandler: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var contests = [Contest]()
    var itemClickHandler: ((Contest) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Debugging Contest with contests size: \(contests.count)")
        return contests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContestCollectionViewCell.className, for: indexPath) as! ContestCollectionViewCell
        cell.contest = contests[indexPath.item]
        print("Debugging Contest laying out : \(indexPath.item)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemClickHandler(contests[indexPath.item])
    }
}

//MARK: - Staggered Layout Delegate
extension ContestsHandler : StaggeredCollectionViewLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, with columnWidth: CGFloat) -> CGFloat {
        return contests[indexPath.item].size == "Small" ? columnWidth : columnWidth * 2
    }
}

// Mark: ContestCollectionViewCell
class ContestCollectionViewCell: UICollectionViewCell {
    
    var contest: Contest! {
        didSet {
            self.fillUI()
        }
    }
    private lazy var stackBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.3
        return view
    }()
    
    @IBOutlet weak var contestImageView: UIImageView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var progressBar: ContestProgressBar!
    
    override func awakeFromNib() {
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 1, left: 7, bottom: 10, right: 7)
        pinBackground(stackBackgroundView, to: stackView)
        titleLabel.backgroundColor = UIColor.clear
        descriptionLabel.backgroundColor = UIColor.clear
        setBoldFont([bannerLabel], 15, .white)
        setSemiBoldFont([titleLabel], 15, .white)
        setLightFont([descriptionLabel], 13, .white)
    }
    
    private func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
    
    private func fillUI() {
        changeCardColor()
        setProgress()
        contestImageView.kf.indicatorType = .activity
        let placeholderName = contest.image == "Small" ? "square" : "rectangle"
        contestImageView.kf.setImage(with: URL(string: contest.image), placeholder:  UIImage(named: placeholderName)) { [weak self] result in
            switch result {
            case .success(_):
                self?.contestImageView.cropToBounds()
            case .failure(let error):
                print(error)
            }
        }
        titleLabel.text = contest.title
        descriptionLabel.text = contest.description
    }
    
    private func changeCardColor() {
        guard let contestType = contest.type else {
            bannerView.isHidden = true
            return
        }
        if contestType == .LOTTERY {
            leftImage.image = #imageLiteral(resourceName: "time_red")
            rightImage.image = #imageLiteral(resourceName: "ribbon_red")
            progressBar.bgColor = .cdf4f41
            bannerLabel.text = "QUEDAN POCAS!"
        }
        else if contestType == .SPECIAL_AUCTION {
            leftImage.image = #imageLiteral(resourceName: "time_blue")
            rightImage.image = #imageLiteral(resourceName: "spl_auction_item")
            progressBar.bgColor = .c313fa7
            bannerLabel.text = "QUEDAN POCAS!)"
        }
        else if contestType == .TRIVIALITIES {
            leftImage.image = #imageLiteral(resourceName: "time_light_blue")
            rightImage.image = #imageLiteral(resourceName: "trivia_icon")
            progressBar.bgColor = .c48b2d2
            bannerLabel.text = "COMIENZA PRONTO!"
        }
        else {
            leftImage.image = #imageLiteral(resourceName: "time_purple")
            rightImage.image = #imageLiteral(resourceName: "ribbon_purple")
            progressBar.bgColor = .c9331a7
            bannerLabel.text = "FALTA POCO!"
        }
        bannerView.backgroundColor = progressBar.bgColor
    }
    
    private func setProgress() {
        guard let contestType = contest.type else {
            return
        }
        var progress: Float = 0
        var showBanner = false
        
        if contestType == .SPECIAL_AUCTION || contestType == .LOTTERY {
            let ticketDiv = Float(contest.ticketSold) / Float(contest.ticketTotal)
            progress = ticketDiv * 100
            showBanner = progress > 85
        }
        else {
            if let serverTime = contest.data?.startTime {
                let currentTime = Date().toMillis()
                let diffMillis = serverTime - currentTime
                let diffSec = diffMillis / 1000;
                let min = diffSec / 60;
                let hours = min / 60;
                if hours < 24 {
                    progress = 100 - ((Float(hours) / 24) * 100)
                }
                showBanner = hours <= 3;
            }
        }
        progressBar.progress = max(progress, 10)
        bannerView.isHidden = !showBanner
    }
}
