//
//  Storyboarded.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 08/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate(storyboardName storyboard: String) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(storyboardName storyboard: String) -> Self {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(self)
        
        // this splits by the dot and uses everything after, giving "MyViewController"
        let splitResult = fullName.components(separatedBy: ".")
        let className = splitResult[splitResult.count - 1]
        
        // load our storyboard
        let storyboard = UIStoryboard(name: storyboard, bundle: Bundle.main)
        
        // instantiate a view controller with that identifier, and force cast as the type that was requested
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}
