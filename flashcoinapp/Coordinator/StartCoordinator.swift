//
//  AuthCoordinator.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 08/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class StartCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    let STORYBOARD_NAME = "Start"
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        //if user already logged in, push to tab bar controller
        if Login.getAccessToken() != nil {
            pushToLandingTabBarController()
        } else {
            
            let vc = StartViewController.instantiate(storyboardName: STORYBOARD_NAME)
            navigationController.pushViewController(vc, animated: true)
        }
    }
    
    func openTermsAndCondition(this: UIViewController) {
        let vc = TermsAndConditionViewController.instantiate(storyboardName: STORYBOARD_NAME)
        this.present(vc, animated: true)
    }
    
    func pushToLandingTabBarController() {
        let vc = LandingTabBarViewController.instantiate(storyboardName: Constants.Storyboard.Main)
        pushSingleViewController(vc: vc)
    }
    
    func logout() {
        let vc = StartViewController.instantiate(storyboardName: STORYBOARD_NAME)
        pushSingleViewController(vc: vc)
    }
    
    func pushSingleViewController(vc: UIViewController) {
        var navigationArray = navigationController.viewControllers
        if navigationArray.count > 0 {
            navigationArray.remove(at: (navigationArray.count) - 1)
            navigationArray.append(vc)
            navigationController.viewControllers = navigationArray
        } else {
            navigationController.pushViewController(vc, animated: true)
        }
    }
}
