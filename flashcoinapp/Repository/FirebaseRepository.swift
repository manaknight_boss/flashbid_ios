//
//  FirebaseRepository.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 01/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import RxSwift
import Firebase

class FirebaseRepository {
    
    private init() { }
    static let shared = FirebaseRepository()
    
    public func getContestObservable(forEventType: DataEventType) -> Observable<Contest> {
        return Database.database().reference()
            .child(FirebaseNode.ALL_ITEMS)
            .child(FirebaseNode.CONTESTS)
            .rx
            .observeEvent(forEventType)
            .map({ (snapshot) -> Contest in
                return Contest.newFromDataSnapshot(snapshot)
            })
    }
    
    public func getContestObservable(forEventType: DataEventType, firebaseKey: String) -> Observable<Contest> {
        return Database.database().reference()
            .child(FirebaseNode.ALL_ITEMS)
            .child(FirebaseNode.CONTESTS)
            .child(firebaseKey)
            .rx
            .observeEvent(forEventType)
            .map({ (snapshot) -> Contest in
                return Contest.newFromDataSnapshot(snapshot)
            })
    }
    
    public func getContestObservable(forEventType: DataEventType, id: Int) -> Observable<Contest> {
        return Database.database().reference()
            .child(FirebaseNode.ALL_ITEMS)
            .child(FirebaseNode.CONTESTS)
            .rx
            .observeEvent(forEventType)
            .concatMap({ (snapshot) -> Observable<DataSnapshot?> in
                return Observable.from(snapshot.iterator())
            })
            .filterNil()
            .map({ (snapshot) -> Contest in
                return Contest.newFromDataSnapshot(snapshot)
            })
            .filter({ (contest) -> Bool in
                return contest.id == id
            })
    }
    
    public func getContestObservable<T>(forEventType: DataEventType, firebaseKey: String, forKey: String, valueType: T.Type) -> Observable<T> {
        return Database.database().reference()
            .child(FirebaseNode.ALL_ITEMS)
            .child(FirebaseNode.CONTESTS)
            .child(firebaseKey)
            .child(forKey)
            .rx
            .observeEvent(forEventType)
            .map({ (snapshot) -> T in
                return snapshot.value as! T
            })
    }
    
    public func getProfileObservable(forEventType: DataEventType) -> Observable<DataSnapshot> {
        return  Database.database().reference()
            .child(FirebaseNode.USER)
            .child(Login.getUserIdString())
            .rx
            .observeEvent(forEventType)
    }
    
    public func getResultObservable(forEventType: DataEventType) -> Observable<Result> {
        return Database.database().reference()
            .child(FirebaseNode.RESULTS)
            .child(FirebaseNode.RESULTS_FIRST_CHILD)
            .rx
            .observeEvent(forEventType)
            .map({ (snapshot) -> Result in
                print(snapshot)
                return Result.newFromDataSnapshot(snapshot)
            })
    }
}
