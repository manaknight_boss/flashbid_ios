//
//  SendStickerOtpDataRepository.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 06/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//


import RxSwift

class SendStickerOtpDataRepository: BaseRepository {
    
    private override init() { }
    static let shared = SendStickerOtpDataRepository()
    
    public func sendStickerOtpData(accessToken: String, id: Int, codeType:String, code: String, amount:Double, vendorId:Int, _ onSucces: @escaping (SendStickerOtpActionResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.sticker, accessToken: accessToken, dict: [
            "id": id,
            "type":  codeType,
            "code": code,
            "amount": amount,
            "vendor_id": vendorId,
            "lang": "sp"
            ], onSucces, onError)
    }
}
