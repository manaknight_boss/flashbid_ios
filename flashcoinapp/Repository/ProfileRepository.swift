//
//  ProfileRepository.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class ProfileRepository: BaseRepository {
    
    private override init() { }
    static let shared = ProfileRepository()
    
    public func actionProfile(accessToken: String, _ onSucces: @escaping (ProfileResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "profile",
            "parameter": "{}",
            "response": [
                "profile_image",
                "email",
                "first_name",
                "last_name",
                "phone_number",
                "address",
                "state",
                "city",
                "country",
                "postal_code",
                "qr_code",
                "data",
                "money_owed",
                "balance"
            ],
            "lang": "sp"
        ], onSucces, onError)
    }
    
    public func actionEditProfile(accessToken: String, request: ActionProfileEditRequest, _ onSucces: @escaping (ProfileResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "profileEdit",
            "parameter": request.encode(),
            "response": [],
            "lang": "sp"
            ], onSucces, onError)
    }
    
    func actionProfileImage(accessToken: String, request: ActionProfileImageRequest, _ onSucces: @escaping (ProfileResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "profileImage",
            "parameter": request.encode(),
            "response": [],
            "lang": "sp"
            ], onSucces, onError)
    }
    
    func downloadProfileImage(imageUrl: String, onComplete: @escaping (UIImage) -> Void) {
        download(from: imageUrl, onComplete)
    }
}
