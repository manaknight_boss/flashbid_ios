//
//  QRScanRepository.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class QRScanRepository: BaseRepository {
    
    private override init() { }
    static let shared = QRScanRepository()
    
    public func actionScannedData(accessToken: String,  request: ActionQRScanRequest, _ onSucces: @escaping (QRScanResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "checkCode",
            "parameter": request.encode(),
            "response": [],
            "lang": "sp"
            ], onSucces, onError)
    }
    
    public func getStickerDataDetails(accessToken: String, responseParam: Array<Any>, request: GetStickerDataRequest, _ onSucces: @escaping (QRScanResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": Constants.QRType.sticker,
            "parameter": request.encode(),
            "response": responseParam,
            "lang": "sp"
            ], onSucces, onError)
    }
    
    public func getPayOtpDataDetails(accessToken: String, responseParam: Array<Any>, request: GetPayOtpDataRequest, _ onSucces: @escaping (GetPayOtpDetailsResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "flashpay",
            "parameter": request.encode(),
            "response": responseParam,
            "lang": "sp"
            ], onSucces, onError)
    }
    
    public func payOtpBillDataDetails(accessToken: String, request: PayOtpBillDataRequest, _ onSucces: @escaping (PayOtpBillDataResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.pay, accessToken: accessToken, dict: [
            "id": request.id,
            "point": request.point,
            "amount": request.amount,
            "lang": "sp"
            ], onSucces, onError)
    }

}
