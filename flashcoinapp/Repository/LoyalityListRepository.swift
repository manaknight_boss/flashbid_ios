//
//  LoyalityListRepository.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class LoyalityListRepository: BaseRepository {
    
    public func actionLoyalityList(accessToken: String, _ onSucces: @escaping (LoyalityListResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "loyalty",
            "parameter": "{}",
            "response": ["id",
                         "company_name",
                         "points"],
            "lang": "sp"
            ], onSucces, onError)
    }
}
