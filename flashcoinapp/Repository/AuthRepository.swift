//
//  AuthRepository.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 10/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class AuthRepository: BaseRepository {
    
    private override init() { }
    static let shared = AuthRepository()
    
    public func login(request: LoginRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let url = URL(string: BaseRepository.baseURL + "login")!
        let dict = [
            "email": request.email,
            "password": request.password
        ]
        makePostCall(url: url, dict: dict, onSucces, onError)
    }
    
    public func facebookLogin(request: FacebookLoginRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let url = URL(string: BaseRepository.baseURL + "login/oauth/facebook")!
        let dict = [
            "email": request.email,
            "first_name": request.firstName,
            "last_name": request.lastName,
            "access_token": request.accessToken
        ]
        makePostCall(url: url, dict: dict, onSucces, onError)
    }
    
    public func register(request: RegisterRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let url = URL(string: BaseRepository.baseURL + "register")!
        let dict = [
            "email": request.email,
            "password": request.password,
            "first_name": request.firstName,
            "last_name": request.surName,
            "phone_number": request.phone
        ]
        makePostCall(url: url, dict: dict, onSucces, onError)
    }
    
    public func forgot(request: ForgotPasswordRequest,_ onSucces: @escaping (MessageResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let url = URL(string: BaseRepository.baseURL + "forgot")!
        let dict = [
            "email": request.email
        ]
        makePostCall(url: url, dict: dict, onSucces, onError)
    }
    
    public func reset(request: UpdatePasswordRequest,_ onSucces: @escaping (MessageResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let url = URL(string: BaseRepository.baseURL + "reset")!
        let dict = [
            "code": request.code,
            "password": request.password
        ]
        makePostCall(url: url, dict: dict, onSucces, onError)
    }
    
    public func sendDeviceId(accessToken: String, firebaseToken: String,_ onSucces: @escaping (SuccessResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        let parameter = "{\"device_id\": \"\(firebaseToken)\", \"device_type\":\"android\"}"
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "device",
            "parameter": parameter,
            "response": [],
            "lang": "sp"
        ], onSucces, onError)
    }
    
    public func actionLogOut(accessToken: String, _ onSucces: @escaping (MessageResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "logout",
            "parameter": "{}",
            "response": [],
            "lang": "sp"
        ], onSucces, onError)
    }
}
