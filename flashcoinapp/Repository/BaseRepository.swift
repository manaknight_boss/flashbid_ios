//
//  BaseRepository.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 10/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class BaseRepository {
    
    public static let baseURL = "https://l5ncvtsg8i.execute-api.us-east-1.amazonaws.com/dev/"
    private static let somthingWentWrongErrorResponse = ErrorResponse(message:  LocalizedString.SOMETHING_WENT_WRONG, statusCode: 403)
    
    // MARK: Api calls
    public func apiCall<T: Decodable>(serviceType:String, accessToken: String, dict: [String: Any], _ onSuccess: @escaping (T) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        BaseRepository.callInBackground(accessToken: accessToken, url:  URL(string: BaseRepository.baseURL + serviceType)!, dict: dict, onSuccess, onError)
    }
    
    public func makePostCall<T: Decodable>(url: URL, dict: [String: Any], _ onSuccess: @escaping (T) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        BaseRepository.callInBackground(url: url, dict: dict, onSuccess, onError)
    }
    
    // MARK: Image calls
    func download(from url: URL, _ onComplete: @escaping (UIImage) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                onComplete(image)
            }
            }.resume()
    }
    
    func download(from link: String, _ onComplete: @escaping (UIImage) -> Void) {
        guard let url = URL(string: link) else { return }
        download(from: url, onComplete)
    }
    
    // MARK: Private helper functions. Please use callInBackground only.
    private static func callInBackground<T: Decodable>(accessToken: String? = nil, url: URL, dict: [String: Any], _ onSuccess: @escaping (T) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        if !Connection.isConnectedToNetwork() {
            onError(ErrorResponse(message: LocalizedString.MESSAGE_INTERNET_NOT_CONNECTED, statusCode: 403))
        } else {
            DispatchQueue.global(qos: .userInitiated).async {
                BaseRepository.call(accessToken: accessToken, url: url, dict: dict, onSuccess, onError)
            }
        }
    }
    
    private static func call<T: Decodable>(accessToken: String? = nil, url: URL, dict: [String: Any], _ onSuccess: @escaping (T) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        print("\n\n")
        
        log("Calling an api using post method: \(url.absoluteString)")
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let json: Data
        do {
            json = try JSONSerialization.data(withJSONObject: dict, options: [])
            
            log("json: \(dict)")
            
            request.httpBody = json
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            log("application/json : Content-Type")
            
            if let accessToken = accessToken {
                request.addValue(accessToken, forHTTPHeaderField: "Authorization")
                
                log("Authorization : \(accessToken)")
            }
        } catch {
            DispatchQueue.main.async {
                onError(self.somthingWentWrongErrorResponse)
            }
            
            log("Error: cannot create JSON from todo")
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            self.handleResponse(data: data, response: response, error: error, onSuccess, onError)
        }
        task.resume()
    }
    
    private static func handleResponse<T: Decodable>(data: Data?, response: URLResponse?, error: Error?, _ onSuccess: @escaping (T) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        guard error == nil else {
            DispatchQueue.main.async {
                onError(self.somthingWentWrongErrorResponse)
            }
            
            log("Error: " + error.debugDescription)
            return
        }
        guard let responseData = data else {
            DispatchQueue.main.async {
                onError(self.somthingWentWrongErrorResponse)
            }
            
            log("Response Data is nil")
            return
        }
        // parse the result as JSON
        do {
            guard let responseDict = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] else {
                self.log("Could not get JSON from responseData as dictionary")
                return
            }
            self.log("The response is: " + responseDict.description)
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let errorResponse = (try decoder.decode(ErrorResponse.self, from: responseData))
            
            log("errorResponse: \(String(describing: errorResponse))")
            
            if errorResponse.statusCode == 403 || (errorResponse.statusCode == nil && errorResponse.message != nil) {
                DispatchQueue.main.async {
                    onError(errorResponse)
                }
                return
            }
            let successResponse = try decoder.decode(T.self, from: responseData)
            
            log("successResponse: \(String(describing: successResponse))")
            
            DispatchQueue.main.async {
                onSuccess(successResponse)
            }
        } catch let err {
            DispatchQueue.main.async {
                onError(self.somthingWentWrongErrorResponse)
            }
            
            log("Err: " + err.localizedDescription)
        }
    }
    
    // MARK: Logging
    private static func log(_ msg: String) {
        printDebug("\nBaseRepository ===> \(msg)")
    }
}
