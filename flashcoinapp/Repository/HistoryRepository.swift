//
//  HistoryRepository.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class HistoryRepository: BaseRepository {
    
    public func actionHistory(accessToken: String, id: Int, _ onSucces: @escaping (ActionHistoryResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType:Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "history",
            "parameter":  "{\"id\":\"\(id)\",\"limit\":20}",
            "response": [],
            "lang": "sp"
        ], onSucces, onError)
    }
}
