//
//  ContestRepository.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//


import RxSwift

class ContestRepository: BaseRepository {
    
    private override init() { }
    static let shared = ContestRepository()
    
    public func actionItem(accessToken: String, request: ItemRequest, isOtp: Bool, _ onSucces: @escaping (ItemResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        var actionName = ""
        if isOtp {
            actionName = "otherItem"
        }
        else{
            actionName = "item"
        }
        
        let responseArray = isOtp
            ? [
                "title", "description", "image", "type", "amount", "stock", "commission", "tax", "discount", "data", "status", "delivery", "id", "full_description", "shop_title", "shop_country", "shop_image", "other_images", "user_like"
                ]
            : [
                "id",
                "title",
                "description",
                "full_description",
                "image",
                "secondary_image",
                "type",
                "initial_bid",
                "final_bid",
                "bid_amount",
                "market_value",
                "buy_at_amount",
                "stock",
                "ticket_amount",
                "ticket_total",
                "ticket_sold",
                "winner_id",
                "room_id",
                "data",
                "status",
                "created_at",
                "shop_title",
                "shop_country",
                "shop_image",
                "other_images",
                "user_like"
        ]
        apiCall(serviceType: Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": actionName,
            "parameter": request.encode(),
            "response": responseArray,
            "lang": "sp"
            ], onSucces, onError)
    }
    
    public func actionTicket(accessToken: String, request: ActionTicketRequest, _ onSucces: @escaping (ActionTicketResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType: Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "ticket",
            "parameter": request.encode(),
            "response": [],
            "lang": "sp"
            ], onSucces, onError)
    }
    
    public func buy(accessToken: String, id: Int, type: String, _ onSucces: @escaping (ActionTicketResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        let dict = [
            "lang": "sp",
            "id": id,
            "type": type
            ] as [String : Any]
        apiCall(serviceType: Constants.serviceType.buy, accessToken: accessToken, dict: dict, onSucces, onError)
    }
    
    public func likeItem(accessToken: String, request: ActionLikeItemRequest, _ onSucces: @escaping (ActionLikeItemResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        apiCall(serviceType: Constants.serviceType.query, accessToken: accessToken, dict: [
            "action": "like",
            "parameter": request.encode(),
            "response": [],
            "lang": "sp"
            ], onSucces, onError)
    }
}
