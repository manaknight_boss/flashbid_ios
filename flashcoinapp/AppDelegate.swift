//
//  AppDelegate.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 08/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import FacebookCore
import Firebase
import UserNotifications
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var firebaseToken: String? {
        get {
            return Login.getFirebaseToken()
        }
        set {
            // We never assign a nil token..
            Login.saveFirebaseToken(firebaseToken: newValue!)
        }
    }
    var accessToken: String? {
        get {
            return Login.getAccessToken()
        }
    }
    
    var window: UIWindow?
    // create the main navigation controller to be used for our app
    // send that into our coordinator so that it can display view controllers
    var startCoordinator: StartCoordinator = StartCoordinator(navigationController: UINavigationController())
    
    // MARK: ViewModel Initialization
    lazy var startViewModel = StartViewModel()
    lazy var profileViewModel = ProfileViewModel()
    lazy var historyViewModel = HistoryViewModel()
    lazy var qrScanViewModel = QRScanViewModel()
    lazy var loyalityListViewModel = GetLoyalityListViewModel()
    lazy var contestViewModel = ContestViewModel()
    lazy var stickerOtpActionViewModel = SendStickerOtpActionViewModel()
    lazy var resultViewModel = ResultViewModel()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        AnyLanguageBundle.setLanguage("es")
        Fabric.sharedSDK().debug = true
        // set navigation bar attributes
        self.setupForNavigationBarAttributes()
        
        // Use Firebase library
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        // Use Facebook SDK
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        startCoordinator.navigationController.isNavigationBarHidden = true
        
        startCoordinator.start()
        
        // create a basic UIWindow and activate it
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = startCoordinator.navigationController
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    // MARK: Facebook
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return SDKApplicationDelegate.shared.application(application,
                                                         open: url,
                                                         sourceApplication: sourceApplication,
                                                         annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        return SDKApplicationDelegate.shared.application(application, open: url, options: options)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    
    private func saveTokenCallApi(fcmToken: String) {
        var requiredRefresh = true
        // Check if token exists in database or its a first time this function is called
        if let firebaseTokenInDB = self.firebaseToken {
            // If token same then no need to update it on server or local but if differ then update on both.
            if fcmToken == firebaseTokenInDB {
                requiredRefresh = false
            }
        } else {
            // Save token when it is generated for first time.
            self.firebaseToken = fcmToken
        }
        // If logged in and required refresh then update token locally after updating it to server
        if let accessToken = self.accessToken, requiredRefresh {
            AuthRepository.shared.sendDeviceId(accessToken: accessToken, firebaseToken: fcmToken, {_ in
                self.firebaseToken = fcmToken
            }, {error in
                printDebug("Error: when sending new firebaseToken \(error)")
            })
        }
    }
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        saveTokenCallApi(fcmToken: fcmToken)
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    // MARK:- Setup for Navigation Bar Attributes
    func setupForNavigationBarAttributes(){
        // Change status bar color
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = .c1f87ae
        }
        
        // Change navigation bar color
        UINavigationBar.appearance().barTintColor = .c1f87ae
        UINavigationBar.appearance().isTranslucent = false
        
        // hide back button titles
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -500, vertical: 0), for:UIBarMetrics.default)
    }
}

