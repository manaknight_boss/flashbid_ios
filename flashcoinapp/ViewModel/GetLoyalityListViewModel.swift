//
//  GetLoyalityListViewModel.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class GetLoyalityListViewModel: BaseViewModel {
    
    public func getLoyalityList(accessToken: String, _ onSucces: @escaping (LoyalityListResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        
        LoyalityListRepository().actionLoyalityList(accessToken: accessToken, onSucces, onError)
        
    }
}
