//
//  QRScanViewModel.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class QRScanViewModel: BaseViewModel {
    
    public func sendScannedData(accessToken: String, request: ActionQRScanRequest, _ onSucces: @escaping (QRScanResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        QRScanRepository.shared.actionScannedData(accessToken: accessToken, request: request, onSucces, onError)
    }
    
    public func callToGetStickerDataDetails(accessToken: String, responseParam: Array<Any>, request: GetStickerDataRequest, _ onSucces: @escaping (QRScanResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        QRScanRepository.shared.getStickerDataDetails(accessToken: accessToken, responseParam: responseParam, request: request, onSucces, onError)
    }
    
    public func callToGetPayOtpDataDetails(accessToken: String, responseParam: Array<Any>, request: GetPayOtpDataRequest, _ onSucces: @escaping (GetPayOtpDetailsResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        QRScanRepository.shared.getPayOtpDataDetails(accessToken: accessToken, responseParam: responseParam, request: request, onSucces, onError)
    }
    
    public func callToPayOtpBillDataDetails(accessToken: String, request: PayOtpBillDataRequest, _ onSucces: @escaping (PayOtpBillDataResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        QRScanRepository.shared.payOtpBillDataDetails(accessToken: accessToken, request: request, onSucces, onError)
    }
    
}
