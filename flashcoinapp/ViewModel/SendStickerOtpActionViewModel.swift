//
//  SendStickerOtpActionViewModel.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 06/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import RxSwift

class SendStickerOtpActionViewModel: BaseViewModel {
    
    public func sendStickerOtpData(accessToken: String, id: Int, codeType:String, code: String, amount:Double, vendorId:Int, _ onSucces: @escaping (SendStickerOtpActionResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        SendStickerOtpDataRepository.shared.sendStickerOtpData(accessToken: accessToken, id: id, codeType: codeType, code: code, amount: amount, vendorId: vendorId, onSucces, onError)
        
    }
}
