//
//  ProfileViewModel.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift
import Firebase

class ProfileViewModel: BaseViewModel {
    
    let image = Variable<UIImage?>(Constants.Images.defaultImage)
    let profileData = Variable<ProfileResponse?>(nil)
    var qrCode: String?
    
    private var isImageDownloaded: Bool = false
    
    // MARK: Private functions
    private func profileResponseSuccess(response: ProfileResponse, onSuccess: @escaping () -> Void) {
        profileData.value = response
        
        if !isImageDownloaded {
            ProfileRepository.shared.download(from: response.profileImage, { (image) in
                self.isImageDownloaded = true
                self.image.value = image
            })
        }
        if qrCode == nil, let code = response.qrCode {
            qrCode = code.base32EncodedString
        }
        onSuccess()
    }
    
    // MARK: Public functions
    public func getProfile(accessToken: String, _ onSuccess: @escaping () -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        
        ProfileRepository.shared.actionProfile(accessToken: accessToken, { response in
            
            self.profileResponseSuccess(response: response, onSuccess: onSuccess)
            
        }, onError)
    }
    
    public func editProfile(accessToken: String, request: ActionProfileEditRequest, _ onSuccess: @escaping () -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        ProfileRepository.shared.actionEditProfile(accessToken: accessToken, request: request, { response in
            
            self.profileResponseSuccess(response: response, onSuccess: onSuccess)
            
        }, onError)
    }
    
    public func uploadProfileImage(accessToken: String, image: UIImage, _ onSuccess: @escaping () -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        
        AWSUploadImageHelper.uploadImage(myImage: image, onSuccess: { awsImageUrl in
            guard !awsImageUrl.isEmpty else {
                onError(ErrorResponse(message: "Couldn't be able to upload image to server", statusCode: 100))
                return
            }
            
            let request = ActionProfileImageRequest(profileImage: awsImageUrl)
            ProfileRepository.shared.actionProfileImage(accessToken: accessToken, request: request, { response in
                
                self.profileResponseSuccess(response: response, onSuccess: onSuccess)
                self.image.value = image
                
            }, onError)
            
        }, onError: onError)
    }
    
    // Mark Firebase
    public func listenProfileChanges() -> Observable<DataSnapshot> {
        return FirebaseRepository.shared
            .getProfileObservable(forEventType: .value)
    }
    
    public func parseFirebaseUser(snapshot: DataSnapshot) {
        if let dict = snapshot.value as? [String: Any] {
            if var profileData = profileData.value {
                profileData.firstName = dict["first_name"] as? String ?? ""
                profileData.lastName = dict["last_name"] as? String ?? ""
                profileData.balance = dict["balance"] as? Double
                self.profileData.value = profileData // Triggers updates
            }
        }
    }
}
