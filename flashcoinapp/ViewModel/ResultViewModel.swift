//
//  ResultViewModel.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import RxSwift
import Firebase

class ResultViewModel: BaseViewModel {
    
    // MARK: Public functions
    public func listenForResult() -> Observable<Result> {
        return Observable.merge(
            FirebaseRepository.shared.getResultObservable(forEventType: .childAdded),
            FirebaseRepository.shared.getResultObservable(forEventType: .childChanged)
        )
    }
}
