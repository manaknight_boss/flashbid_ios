//
//  ContestViewModel.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 01/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import RxSwift

class ContestViewModel: BaseViewModel {
    
    // MARK: Public functions
    public func listenForContest() -> Observable<Contest> {
        return Observable.merge(
            FirebaseRepository.shared.getContestObservable(forEventType: .childAdded),
            FirebaseRepository.shared.getContestObservable(forEventType: .childChanged)
            )
    }
    
    public func listenForContest(firebaseKey: String) -> Observable<Contest> {
        return FirebaseRepository.shared.getContestObservable(forEventType: .value, firebaseKey: firebaseKey)
    }
    
    public func listenForContest(id: Int) -> Observable<Contest> {
        return FirebaseRepository.shared.getContestObservable(forEventType: .value, id: id)
    }
    
    public func listenForTicketSold(firebaseKey: String) -> Observable<Int> {
        return FirebaseRepository.shared.getContestObservable(forEventType: .value, firebaseKey: firebaseKey, forKey: "ticket_sold", valueType: Int.self)
    }
    
    public func getContestDetail(accessToken: String, request: ItemRequest, isOtp: Bool, _ onSucces: @escaping (ItemResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        ContestRepository.shared.actionItem(accessToken: accessToken, request: request, isOtp: isOtp, onSucces, onError)
    }
    
    public func buyTicket(accessToken: String, request: ActionTicketRequest, _ onSucces: @escaping (ActionTicketResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        ContestRepository.shared.actionTicket(accessToken: accessToken, request: request, onSucces, onError)
    }
    
    public func buyOtp(accessToken: String, id: Int, type: String, _ onSucces: @escaping (ActionTicketResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        ContestRepository.shared.buy(accessToken: accessToken, id: id, type: type, onSucces, onError)
    }
    public func likeItem(accessToken: String, request: ActionLikeItemRequest, _ onSucces: @escaping (ActionLikeItemResponse) -> Void, _ onError: @escaping (ErrorResponse) -> Void) {
        ContestRepository.shared.likeItem(accessToken: accessToken, request: request, onSucces, onError)
    }
}
