//
//  HomeViewModel.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 17/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class HomeViewModel: BaseViewModel {
    
    public func logOut(accessToken: String, _ onSucces: @escaping (MessageResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        
        AuthRepository.shared.actionLogOut(accessToken: accessToken, onSucces, onError)
    }
}
