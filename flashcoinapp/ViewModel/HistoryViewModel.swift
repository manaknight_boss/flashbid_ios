//
//  HistoryViewModel.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class HistoryViewModel: BaseViewModel {
    
    public func getHistory(accessToken: String, id: Int, _ onSucces: @escaping (ActionHistoryResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        
        HistoryRepository().actionHistory(accessToken: accessToken, id: id, onSucces, onError)
        
    }
}
