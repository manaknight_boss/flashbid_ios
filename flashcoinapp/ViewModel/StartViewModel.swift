//
//  StartViewModel.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 10/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import RxSwift

class StartViewModel: BaseViewModel {
    
    public func login(firebaseToken: String, request: LoginRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        
        AuthRepository.shared.login(request: request, { tokenIdResponse in
            AuthRepository.shared.sendDeviceId(accessToken: tokenIdResponse.accessToken, firebaseToken: firebaseToken, {_ in
                onSucces(tokenIdResponse)
            }, onError)
        }, onError)
    }
    
    public func loginWithFacebook(firebaseToken: String, request: FacebookLoginRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        AuthRepository.shared.facebookLogin(request: request, { tokenIdResponse in
            AuthRepository.shared.sendDeviceId(accessToken: tokenIdResponse.accessToken, firebaseToken: firebaseToken, {_ in
                onSucces(tokenIdResponse)
            }, onError)
        }, onError)
    }
    
    public func register(firebaseToken: String, request: RegisterRequest,_ onSucces: @escaping (TokenUserIdResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        AuthRepository.shared.register(request: request, { tokenIdResponse in
            AuthRepository.shared.sendDeviceId(accessToken: tokenIdResponse.accessToken, firebaseToken: firebaseToken, {_ in
                onSucces(tokenIdResponse)
            }, onError)
        }, onError)
    }
    
    public func forgotPassword(request: ForgotPasswordRequest,_ onSucces: @escaping (MessageResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        AuthRepository.shared.forgot(request: request, onSucces, onError)
    }
    
    public func updatePassword(request: UpdatePasswordRequest,_ onSucces: @escaping (MessageResponse) -> Void,_ onError: @escaping (ErrorResponse) -> Void) {
        AuthRepository.shared.reset(request: request, onSucces, onError)
    }
}
