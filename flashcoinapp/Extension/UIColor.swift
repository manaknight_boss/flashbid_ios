//
//  ColorHelper.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var cdf4f41: UIColor {
        return UIColor(hex: "df4f41")
    }
    class var c313fa7: UIColor {
        return UIColor(hex: "313fa7")
    }
    class var c48b2d2: UIColor {
        return UIColor(hex: "48b2d2")
    }
    class var c9331a7: UIColor {
        return UIColor(hex: "9331a7")
    }
    class var cc0b8b8: UIColor {
        return UIColor(hex: "c0b8b8")
    }
    class var c1f87ae: UIColor {
        return UIColor(hex: "1f87ae")
    }
    class var ce2922b: UIColor {
        return UIColor(hex: "e2922b")
    }
    class var c919090: UIColor {
        return UIColor(hex: "919090")
    }
    class var ce7e7e7: UIColor {
        return UIColor(hex: "e7e7e7")
    }
    class var c999999: UIColor {
        return UIColor(hex: "999999")
    }
    class var c212121: UIColor {
        return UIColor(hex: "212121")
    }
    class var cc44242: UIColor {
        return UIColor(hex: "c44242")
    }
    class var c80000000: UIColor {
        return UIColor(hex: "80000000")
    }
    class var c202020: UIColor {
        return UIColor(hex: "202020")
    }
    
    convenience init(hex: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        var hex:   String = hex
        
        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }
        
        let scanner = Scanner(string: hex)
        var hexValue: CUnsignedLongLong = 0
        if scanner.scanHexInt64(&hexValue) {
            switch (hex.count) {
            case 3:
                red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                blue  = CGFloat(hexValue & 0x00F)              / 15.0
            case 4:
                red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                alpha = CGFloat(hexValue & 0x000F)             / 15.0
            case 6:
                red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
            case 8:
                red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
            default:
                printDebug("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
            }
        } else {
            printDebug("Scan hex error")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
