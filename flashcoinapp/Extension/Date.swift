//
//  Date.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 21/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Foundation

extension Date {
    
    func toMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
    static var now: Date {
        return Date.init(timeIntervalSinceNow: 0)
    }
    
    var startOfWeek: Date? {
        return Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    
    func yearsTo(_ date : Date) -> Int{
        return Calendar.current.dateComponents([.year], from: self, to: date).year ?? 0
    }
    
    func isSameDay(as date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs: date)
    }
    
    func isTodayDate() -> Bool {
        return isSameDay(as: Date.now)
    }
    
    func isYesterdayDate() -> Bool {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let str = df.string(from: .now)
        let newDate = df.date(from: str)!
        return isSameDay(as: newDate)
    }
}

extension TimeInterval {
    
    var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    var seconds: Int {
        return Int(self) % 60
    }
    
    var minutes: Int {
        return (Int(self) / 60 ) % 60
    }
    
    var hours: Int {
        return Int(self) / 3600
    }
    
    var stringTime: String {
        if hours != 0 {
            return "\(hours)h \(minutes)m \(seconds)s"
        } else if minutes != 0 {
            return "\(minutes)m \(seconds)s"
        } else if milliseconds != 0 {
            return "\(seconds)s \(milliseconds)ms"
        } else {
            return "\(seconds)s"
        }
    }
}
