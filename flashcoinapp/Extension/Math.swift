//
//  Math.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 11/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import Foundation

func divide(_ x: Int, by y: Int) -> Float {
    if y == 0 {
        print("Error: Divide by zero.")
        return 0
    }
    return Float(x) / Float(y)
}
