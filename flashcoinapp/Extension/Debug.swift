//
//  Debug.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

// Just comment the print statement when release.
func printDebug<T>(_ obj: T, seperator: String = "", terminator: String = "") {
    print(obj, separator: seperator, terminator: terminator)
}


