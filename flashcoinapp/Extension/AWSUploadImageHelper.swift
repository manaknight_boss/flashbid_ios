//
//  AWSUploadImageHelper.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import AWSS3
import AWSCore

class AWSUploadImageHelper {
    
    class func uploadImage(myImage: UIImage, onSuccess: @escaping (String) -> Void, onError: @escaping (ErrorResponse) -> Void) {
        
        if !Connection.isConnectedToNetwork() {
            onError(ErrorResponse(message: LocalizedString.MESSAGE_INTERNET_NOT_CONNECTED, statusCode: 403))
            return
        }
        
        let accessKey = "AKIAIOAE7XB65XOIDRBA"
        let secretKey = "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"
        var awsImageUrl = ""
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let currentTime = Date().toMillis()

        let S3BucketName = "images.flashbid"
        let remoteName = "IMG_" + "\(currentTime)_" + "\(Login.getUserId())"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        let image = myImage
        let data = image.pngData()
        do {
            try data?.write(to: fileURL)
        }
        catch {}
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/png"
        uploadRequest.acl = .publicRead
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest).continueWith(block: { (task: AWSTask) -> Void in
            DispatchQueue.main.async {
                if let error = task.error {
                    AWSUploadImageHelper.log(error.localizedDescription)
                    onError(ErrorResponse(message: LocalizedString.SOMETHING_WENT_WRONG, statusCode: 100))
                }
                else if task.result != nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                    AWSUploadImageHelper.log("Uploaded to:\(publicURL!.absoluteString)")
                    awsImageUrl = publicURL!.absoluteString
                    onSuccess(awsImageUrl)
                }
                else {
                    AWSUploadImageHelper.log("task.result is nil")
                    onError(ErrorResponse(message: LocalizedString.SOMETHING_WENT_WRONG, statusCode: 100))
                }
            }
        })
    }
    
    class func log(_ msg: String) {
        printDebug("\nAWSUploadImageHelper: \(msg)")
    }
}
