//
//  FirebaseExtension.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 18/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Firebase

extension DataSnapshot {
    
    func iterator() -> [DataSnapshot?] {
        return self
            .children
            .allObjects
            .map({ $0 as? DataSnapshot })
    }
    
    func dictIterator() -> [[String: Any]?] {
        return iterator()
            .map({ $0?.value as? [String: Any] })
    }
    
    func intIterator() -> [Int?] {
        return iterator()
            .map({ $0?.value as? Int })
    }
}
