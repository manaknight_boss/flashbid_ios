//
//  DocodableEncodable.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 22/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import Foundation

extension Encodable {
    
    func encode() -> String {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = .convertToSnakeCase
        do {
            let jsonData = try jsonEncoder.encode(self)
             return String(data: jsonData, encoding: .utf8)!
        }
        catch {
            printDebug("extension Encodable failed to encode data.")
        }
        return "extension Encodable failed to encode data."
    }
}

//extension Decodable {
//    
//    static func decode<T: Decodable>(response: Data) -> T? {
//        let decoder = JSONDecoder()
//        decoder.keyDecodingStrategy = .convertFromSnakeCase
//        do {
//            return try decoder.decode(T.self, from: response)
//        } catch {
//            printDebug("extension Decodable failed to decode data.")
//        }
//        return nil
//    }
//}
