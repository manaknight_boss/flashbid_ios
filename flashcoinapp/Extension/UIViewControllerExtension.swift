//
//  UIViewControllerExtension.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit
import RxFirebase
import Firebase
import RxSwift

extension UIViewController: EveryWhere, FontExtension {
    
    // MARK: AppDelegate
    var appDelegate: AppDelegate {
        get { return UIApplication.shared.delegate as! AppDelegate }
    }
    
    var startCoordinator: StartCoordinator {
        get { return appDelegate.startCoordinator }
    }
    
    var firebaseToken: String {
        // It will never happen that token will be null because this token is used in view contoller after login.
        get { return appDelegate.firebaseToken ?? "empty_firebase_token" }
    }
    
    var accessToken: String {
        // It will never happen that token will be null because this token is used in view contoller after login.
        get { return appDelegate.accessToken ?? "empty_access_token" }
    }
    
    var startViewModel: StartViewModel {
        get { return appDelegate.startViewModel }
    }
    
    var profileViewModel: ProfileViewModel {
        get { return appDelegate.profileViewModel }
    }
    
    var historyViewModel: HistoryViewModel {
        get { return appDelegate.historyViewModel }
    }
    
    var qrScanViewModel: QRScanViewModel {
        get { return appDelegate.qrScanViewModel }
    }
    
    var loyalityListViewModel: GetLoyalityListViewModel {
        get { return appDelegate.loyalityListViewModel }
    }
    
    var contestViewModel: ContestViewModel {
        get { return appDelegate.contestViewModel }
    }
    
    var stickerOtpActionViewModel: SendStickerOtpActionViewModel {
        get { return appDelegate.stickerOtpActionViewModel }
    }
    
    var resultViewModel: ResultViewModel {
        get { return appDelegate.resultViewModel }
    }
    
    // MARK: Dialog
    func showSuccessDialog(message: String, _ onOk: ((UIAlertAction) -> Void)? = nil) {
        showDialog(isSuccess: true, message: message, onOk: onOk)
    }
    
    func showErrorDialog(message: String, _ onOk: ((UIAlertAction) -> Void)? = nil) {
        showDialog(isSuccess: false, message: message, onOk: onOk)
    }
    
    private func showDialog(isSuccess: Bool, message: String, onOk: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController.init(title: isSuccess ? LocalizedString.SUCCESS : LocalizedString.ERROR, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: LocalizedString.OK, style: .default, handler: onOk))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Maintenance
    func checkForMaintenance(snapshot: DataSnapshot) {
        snapshot
            .intIterator()
            .forEach({
                if $0 == 1 {
                    showErrorDialog(message: LocalizedString.MESSAGE_MAINTENANCE) { _ in
                        exit(0)
                    }
                }
            })
    }
}
