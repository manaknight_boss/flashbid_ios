//
//  UIView.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 30/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: Child TextFields
    func emptyChildTextFields() {
        self.subviews
            .map({ (view) -> UITextField? in
                return view as? UITextField
            })
            .filter({ $0 != nil })
            .forEach({ $0?.text = "" })
    }
    
    func setIncrementalTagOnEachTextField() {
        var tag = 0
        self.subviews
            .map({ (view) -> UITextField? in
                return view as? UITextField
            })
            .filter({ $0 != nil })
            .map({ $0! })
            .forEach({ $0.tag = tag; tag = tag + 1 })
    }
    
    func setTextFieldDelegateOnEachTextField(delegate: UITextFieldDelegate) {
        self.subviews
            .map({ (view) -> UITextField? in
                return view as? UITextField
            })
            .filter({ $0 != nil })
            .map({ $0! })
            .forEach({ $0.delegate = delegate })
    }
    
    // MARK: Auto Layout
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate(
            [
                leadingAnchor.constraint(equalTo: view.leadingAnchor),
                trailingAnchor.constraint(equalTo: view.trailingAnchor),
                topAnchor.constraint(equalTo: view.topAnchor),
                bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }
}
