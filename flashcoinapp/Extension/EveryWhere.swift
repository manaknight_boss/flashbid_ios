//
//  NSObject.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 22/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

protocol EveryWhere {
}

extension EveryWhere {
    
    static var className: String {
        get {
            return String(describing: self)
        }
    }
}
