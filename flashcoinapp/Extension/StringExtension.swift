//
//  String.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 11/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

extension String {
    
    var isNotEmpty: Bool {
        get {
            return !self.isEmpty
        }
    }
    
    func getUnderLineText(_ underlineText: String) -> NSAttributedString {
        let range = self.range(of: underlineText)!.nsRange
        let attributedText = NSMutableAttributedString(string: self)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        return attributedText
    }
    
    func isValidEmail() -> Bool {
        if self.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isNotValidEmail() -> Bool {
        return !isValidEmail()
    }
    
    func isValidPassword() -> Bool {
        if self.isEmpty {
            return false
        }
        // contain an uppercase, symbol, number and at least 8 letters or and characters.
        let passwordRegEx = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()=_+:;.,/?'\"|`~-]).{8,20})"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func isNotValidPassword() -> Bool {
        return !isValidPassword()
    }
    
    func equalIgnoreCase(_ otherString: String) -> Bool {
        return self.lowercased() == otherString.lowercased()
    }
    
    func equalIgnoreCase(_ otherStringList: [String], atleastOneShouldTrue: Bool = true) -> Bool {
        if otherStringList.count == 0 { return true }
        
        let trueCasesCount = otherStringList
            .filter({ (string) -> Bool in
                return self.equalIgnoreCase(string)
            })
            .count
        return atleastOneShouldTrue ? trueCasesCount > 0 : trueCasesCount == otherStringList.count
    }
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
