//
//  FontHelper.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 09/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

protocol FontExtension {
}

extension FontExtension {
    
    func setBoldFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.bold(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setItalicFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.italic(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setLightFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.light(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setLightItalicFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.lightItalic(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setMediumFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.medium(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setMediumItalicFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.mediumItalic(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setRegularFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.regular(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setSemiBoldFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.semiBold(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    func setSemiBoldItalicFont(_ views: [Any], _ fontSize: CGFloat, _ textColor: UIColor? = nil) {
        let font = FontHelper.semiBoldItalic(fontSize: fontSize)
        setFont(font: font, views: views, textColor: textColor)
    }
    
    private func setFont(font: UIFont, views: [Any?], textColor: UIColor? = nil) {
        let color = textColor
        views.forEach { (view) in
            if let label = view as? UILabel{
                label.font = font
                label.textColor = color
            } else if let button = view as? UIButton {
                button.titleLabel?.font = font
                button.titleLabel?.textColor = color
                button.tintColor = color
            } else if let textField = view as? UITextField {
                textField.font = font
                textField.textColor = color
                textField.tintColor = color
            } else if let textView = view as? UITextView {
                textView.font = font
                textView.textColor = color
            } else if let barButtonItem = view as? UIBarButtonItem {
                barButtonItem.setTitleTextAttributes(font.textAttributes(textColor: textColor), for: .normal)
                barButtonItem.setTitleTextAttributes(font.textAttributes(textColor: textColor), for: .highlighted)
            } else if let navigationBar = view as? UINavigationBar {
                navigationBar.titleTextAttributes = font.textAttributes(textColor: textColor)
            }
        }
    }
}

struct FontHelper {
    
    static let fontDict: [String: String] = [
        "bold": "Exo2-Bold",
        "italic": "Exo2-Italic",
        "light": "Exo2-Light",
        "light_italic": "Exo2-LightItalic",
        "medium": "Exo2-Medium",
        "medium_italic": "Exo2-MediumItalic",
        "regular": "Exo2-Regular",
        "semi_bold": "Exo2-SemiBold",
        "semi_bold_italic": "Exo2-SemiBoldItalic"
    ];
    
    static func bold(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["bold"]!, size: fontSize)!
    }
    
    static func italic(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["italic"]!, size: fontSize)!
    }
    
    static func light(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["light"]!, size: fontSize)!
    }
    
    static func lightItalic(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["light_italic"]!, size: fontSize)!
    }
    
    static func medium(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["medium"]!, size: fontSize)!
    }
    
    static func mediumItalic(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["medium_italic"]!, size: fontSize)!
    }
    
    static func regular(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["regular"]!, size: fontSize)!
    }
    
    static func semiBold(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["semi_bold"]!, size: fontSize)!
    }
    
    static func semiBoldItalic(fontSize: CGFloat) -> UIFont {
        return UIFont(name: fontDict["semi_bold_italic"]!, size: fontSize)!
    }
}

extension UIFont {
    
    func textAttributes(textColor: UIColor? = nil) -> [NSAttributedString.Key: Any] {
        if let textColor = textColor {
            return [
                NSAttributedString.Key.font: self,
                NSAttributedString.Key.foregroundColor: textColor ?? .black
            ]
        }
        return [NSAttributedString.Key.font: self]
    }
}
