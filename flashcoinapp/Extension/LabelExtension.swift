//
//  LabelExtension.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 16/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

extension UILabel {
    
    func underline(_ text: String) {
        self.attributedText = self.text?.getUnderLineText(text)
    }
}
