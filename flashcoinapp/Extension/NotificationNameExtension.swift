//
//  NotificationNameExtension.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 19/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let sideBarItemClicked = Notification.Name("SideBarItemClicked")
}
