//
//  ButtonExtension.swift
//  flashcoinapp
//
//  Created by Vatish Sharma on 08/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    func underline() {
        self.setAttributedTitle(self.title(for: .normal)?.getUnderLineText(self.title(for: .normal) ?? ""), for: .normal)
    }
    
    func download(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, _ onComplete: @escaping (UIImage) -> Void) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.setImage(image, for: .normal)
                onComplete(image)
            }
            }.resume()
    }
    
    func download(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit, _ onComplete: @escaping (UIImage) -> Void) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        download(from: url, contentMode: mode, onComplete)
    }
}
