//
//  Constants.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 18/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//


import Foundation
import UIKit

struct Constants{

    static let defaults = UserDefaults.standard
    
    static let historyTypes = ["Subasta Flash","Sorteo","FP","Info","Tienda","Fidelidad","Subasta","Trivialidades","Fidelidad"]
    
    struct Controller {
        /* Before adding any new controller here, please look at the usage of Storyboarder interface :) */
        static let HistoryViewController = "HistoryViewController"
        static let AboutViewController = "AboutViewController"
        static let LandingTabBarViewController = "LandingTabBarViewController"
        static let QRCodeViewController = "QRCodeViewController"
        static let ProfileViewController = "ProfileViewController"
        static let QRScannerViewController = "QRScannerViewController"
        static let LoyaltyListInServicesViewController = "LoyaltyListInServicesViewController"
        static let UserOtpViewController = "UserOtpViewController"
        static let StickerOtpViewController = "StickerOtpViewController"
        static let PayOtpViewController = "PayOtpViewController"
        static let LogOut = "LogOut"
    }
    
    struct String {
        static let item = "item"
        static let itemName = "itemName"
        static let MyQRCode = "MyQRCode"
        static let warning = "Warning"
    }
    
    struct QRType{
        static let user = "user"
        static let sticker = "sticker"
        static let pay = "pay"
        
        static let fotp = "fotp"
        static let lottery = "lottery"
        static let auction = "auction"
        static let otp = "otp"

    }
    
    struct serviceType{
        static let query = "query"
        static let sticker = "sticker"
        static let buy = "buy"
        static let pay = "pay"
    }
    
    struct Msg {
        static let noCamera = "You don't have camera."
    }
    
    struct Storyboard {
        static let Main = "Main"
        static let Start = "Start"
        static let Profile = "Profile"
        static let Otp = "Otp"
    }
    
    struct ScreenSize {
        static let screenWidth = UIScreen.main.bounds.width
        static let screenHeight = UIScreen.main.bounds.height
    }
    
    struct Images{
        static let defaultImage = UIImage.init(named: "profile_photo")

    }
}
