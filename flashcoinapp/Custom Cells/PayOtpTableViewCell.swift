//
//  PayOtpTableViewCell.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 05/01/19.
//  Copyright © 2019 Flashbid. All rights reserved.
//

import UIKit

class PayOtpTableViewCell: UITableViewCell {
    @IBOutlet weak var itemLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
