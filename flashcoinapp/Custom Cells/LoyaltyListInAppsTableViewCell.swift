//
//  LoyaltyListInAppsTableViewCell.swift
//  flashcoinapp
//
//  Created by Jasanpreet Singh on 27/12/18.
//  Copyright © 2018 Flashbid. All rights reserved.
//

import UIKit

class LoyaltyListInAppsTableViewCell: UITableViewCell {

    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var rowTitleLbl: UILabel!
    @IBOutlet weak var rowImageLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
